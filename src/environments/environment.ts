// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var rootUrl1 = "http://192.168.1.119:8080/hubble-api/";
export const environment = {
  production: false,
  loginURL: rootUrl1 + 'employee/login',
  employeeSearch: rootUrl1 + 'employee/search',
  departmentsURL: rootUrl1 + 'employee/get-all-deparment-locations-designation',
  getEmployee: rootUrl1 + 'employee/get-employee-profile',
  updateEmployeeDetails: rootUrl1 + 'employee/update-employement-details',
  updatePersonalDetails: rootUrl1 + 'employee/update-personal-details',
  updateConfidentialDetails: rootUrl1 + 'employee/update-confedential-details',
  updateSocialDetails: rootUrl1 + 'employee/update-social-details',
  updateSkills: rootUrl1 + 'employee/update-skills-details',
  //updateSkillsCertificationsDetails: rootUrl1 + '/update-skills-certifications-details',
  searchLeavesOfEmployee: rootUrl1 + 'employee/leaves/search',
  applyLeaves: rootUrl1 + 'employee/leaves/add',
  updateLeaves: rootUrl1 + 'employee/leaves/update',
  cancelLeave: rootUrl1 + 'employee/leaves/cancel-leave',
  getTeamMembers: rootUrl1 + 'get-team-members',
  searchLeavesOfSubOrdinates: rootUrl1 + 'team-members-leaves/search',
  availableLeaves: rootUrl1 + 'employee/get-available-leaves',
  individualLeaves: rootUrl1 + "employee/leaves-all/details-by-id"
};
