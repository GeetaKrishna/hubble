import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TapprovalsComponent } from './tapprovals.component';

describe('TapprovalsComponent', () => {
  let component: TapprovalsComponent;
  let fixture: ComponentFixture<TapprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TapprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TapprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
