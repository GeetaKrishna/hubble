import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
import { pagesToggleService } from '../../services/toggler.service';
import { Router } from '@angular/router';
import { RestApiService } from '../../../services/rest-api.service';

@Component({
  selector: 'corporate-layout',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CorporateLayout extends RootLayout implements OnInit {
  userDetails:any;
  fullName: any;

  constructor (public toggler:pagesToggleService,public router: Router,public loginapi: RestApiService){
    super(toggler,router);
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    console.log("user details", this.userDetails);
    
    this.fullName = this.userDetails.FName+ ' ' + this.userDetails.MName + ' ' + this.userDetails.LName;
    //console.log(JSON.parse(this.userDetails))
  }
  menuLinks1 = [
    // {
    //   label:"Dashboard",
    //   routerLink:"social",
    //   iconType:"pg",
    //   iconName:"home"
    // },
    {
      label:"Employee Search",
     // details:"12 New Updates",
      routerLink:"/dashboard/employee-search",
      iconType:"pg",
      iconName:"search",
      thumbNailClass:"text-white"
    },
    {
      label:"Timesheets",
      
      iconType:"pg",
      iconName:"clock",
      toggle:"close",
      submenu:[
        {
          label:"Add",
          routerLink:"/dashboard/timesheets",
          iconType:"pg",
          iconName:"plus_circle",
        },
        {
          label:"Add2",
          routerLink:"/dashboard/timesheets-3",
          iconType:"pg",
          iconName:"plus_circle",
        },
        {
          label:"History",
          routerLink:"/dashboard/timesheets-2",
          iconType:"fa",
          iconName:"history",
        },
        {
          label:"Approval",
          routerLink:"/dashboard/timesheets-approvals",         
          iconType:"fa",
          iconName:"gavel",
        }
       
      ]
    },
    {
      label:"Leaves",
      routerLink:"/dashboard/leaves",
      iconType:"fa",
      iconName:"umbrella",
     },
     {
    label:"Hierarchy",
    routerLink:"/dashboard/hierarchy",
    iconType:"fa",
    iconName:"users"
  },
  {
    label:"Calendar",
    routerLink:"/dashboard/calendar",
    iconType:"fa",
    iconName:"calendar"
  },

    // {
    //   label:"Profile",
    //   routerLink:"social",
    //   iconType:"pg",
    //   iconName:"social"
    // },
    
    
    {
      label:"BMS",
      routerLink:"/dashboard/bms",
      iconType:"pg",
      iconName:"telephone"
     
    },
    {
      label:"Star Performer",
      routerLink:"/dashboard/star-performers",
      iconType:"pg",
      iconName:"telephone"
     
    },
    // {
    //   label:"Holiday Calendar",
    //   routerLink:"social",
    //   iconType:"pg",
    //   iconName:"calender"
    // },
   
  ]
  
  menuLinks = [
      {
        label:"Dashboard",
        details:"12 New Updates",
        routerLink:"dashboard",
        iconType:"pg",
        iconName:"home",
        thumbNailClass:"text-white",
        
      },
      {
          label:"Email",
          details:"234 New Emails",
          routerLink:"email/list",
          iconType:"pg",
          iconName:"mail"
      },
      {
        label:"Social",
        routerLink:"social",
        iconType:"pg",
        iconName:"social"
      },
      {
          label:"Builder",
          routerLink:"builder/corporate-builder",
          iconType:"pg",
          iconName:"layouts"
      },
      {
        label:"Layouts",
        iconType:"pg",
        iconName:"layouts2",
        toggle:"close",
        submenu:[
          {
            label:"Default",
            routerLink:"layouts/default",
            iconType:"letter",
            iconName:"dl",
          },
          {
            label:"Secondary",
            routerLink:"layouts/secondary",
            iconType:"letter",
            iconName:"sl",
          },
          {
            label:"Boxed",
            routerLink:"layouts/boxed-alt",
            iconType:"letter",
            iconName:"bl",
          }
        ]
    },
      {
        label:"UI Elements",
        iconType:"letter",
        iconName:"Ui",
        toggle:"close",
        submenu:[
          {
            label:"Color",
            routerLink:"ui/color",
            iconType:"letter",
            iconName:"c",
          },
          {
            label:"Typography",
            routerLink:"ui/typography",
            iconType:"letter",
            iconName:"t",
          },
          {
            label:"Icons",
            routerLink:"ui/icons",
            iconType:"letter",
            iconName:"i",
          },
          {
            label:"Buttons",
            routerLink:"ui/buttons",
            iconType:"letter",
            iconName:"b",
          },
          {
            label:"Notifications",
            routerLink:"ui/notifications",
            iconType:"letter",
            iconName:"n",
          },
          {
            label:"Modals",
            routerLink:"ui/modal",
            iconType:"letter",
            iconName:"m",
          },
          {
            label:"Progress & Activity",
            routerLink:"ui/progress",
            iconType:"letter",
            iconName:"pa",
          },
          {
            label:"Tabs & Accordians",
            routerLink:"ui/tabs",
            iconType:"letter",
            iconName:"a",
          },
          {
            label:"Sliders",
            routerLink:"ui/sliders",
            iconType:"letter",
            iconName:"s",
          },
          {
            label:"Treeview",
            routerLink:"ui/tree",
            iconType:"letter",
            iconName:"tv",
          }
        ]
      },
      {
          label:"Forms",
          iconType:"pg",
          iconName:"form",
          toggle:"close",
          submenu:[
            {
              label:"Form Elements",
              routerLink:"forms/elements",
              iconType:"letter",
              iconName:"fe",
            },
            {
              label:"Form Layouts",
              routerLink:"forms/layouts",
              iconType:"letter",
              iconName:"fl",
            },
            {
              label:"Form Wizard",
              routerLink:"forms/wizard",
              iconType:"letter",
              iconName:"fq",
            }
          ]
      },
      {
          label:"Cards",
          routerLink:"cards",
          iconType:"pg",
          iconName:"grid"
      },
      {
          label:"Views",
          routerLink:"views",
          iconType:"pg",
          iconName:"ui"
      },
      {
          label:"Tables",
          iconType:"pg",
          iconName:"tables",
          toggle:"close",
          submenu:[
            {
              label:"Basic Tables",
              routerLink:"tables/basic",
              iconType:"letter",
              iconName:"bt",
            },
            {
              label:"Advance Tables",
              routerLink:"tables/advance",
              iconType:"letter",
              iconName:"dt",
            }
          ]
      },
      {
          label:"Maps",
          iconType:"pg",
          iconName:"map",
          toggle:"close",
          submenu:[
            {
              label:"Google Maps",
              routerLink:"maps/google/with-header",
              iconType:"letter",
              iconName:"gm",
            }
          ]
      },
      {
          label:"Charts",
          routerLink:"charts",
          iconType:"pg",
          iconName:"charts"
      },
      {
          label:"Extra",
          iconType:"pg",
          iconName:"bag",
          toggle:"close",
          submenu:[
            {
              label:"Invoice",
              routerLink:"extra/invoice",
              iconType:"letter",
              iconName:"in",
            },
            {
              label:"404 Page",
              routerLink:"session/error/404",
              iconType:"letter",
              iconName:"pg",
            },
            {
              label:"500 Page",
              routerLink:"session/error/500",
              iconType:"letter",
              iconName:"pg",
            },
            {
              label:"Login",
              routerLink:"session/login",
              iconType:"letter",
              iconName:"l",
            },
            {
              label:"Register",
              routerLink:"session/register",
              iconType:"letter",
              iconName:"re",
            },
            {
              label:"Lockscreen",
              routerLink:"session/lock",
              iconType:"letter",
              iconName:"ls",
            },
            {
              label:"Gallery",
              routerLink:"extra/gallery",
              iconType:"letter",
              iconName:"gl",
            },
            {
              label:"Timeline",
              routerLink:"extra/timeline",
              iconType:"letter",
              iconName:"t",
            }
          ]
      },
      {
        label:"Docs",
        externalLink:"https://docs.pages.revox.io/v/angular/",
        target:"_blank",
        iconType:"pg",
        iconName:"note"
      },
      {
        label:"Changelog",
        externalLink:"http://changelog.pages.revox.io/",
        target:"_blank",
        iconType:"letter",
        iconName:"Cl"
      },
  ];

  ngOnInit() {
    this.changeLayout("menu-pin");
    this.changeLayout("menu-behind");
    //Will sidebar close on screens below 1024
    this.autoHideMenuPin();
  }

  fileNameLimit(fileName){

    if(fileName.length>=21){
  
      return fileName.substring(0,19)+'...';
    }else{

      return fileName;
    }
  }

ngOnDestroy(){
  localStorage.removeItem('navCtrlId');
}

  logout(){
    localStorage.clear();
    this.router.navigateByUrl('/');
    this.loginapi.setLoggedIn(false)
  }

}
