import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MessageService } from '../../@pages/components/message/message.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import 'rxjs/add/operator/pairwise';
import { AsYouType } from 'libphonenumber-js';

import {
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { environment } from '../../../environments/environment';
import { RestApiService } from '../../services/rest-api.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';

@Component({
  selector: 'app-profile-new',
  templateUrl: './profile-new.component.html',
  styleUrls: ['./profile-new.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileNewComponent implements OnInit {
  searchUrl = environment.getEmployee;
  employeeUpdateUrl = environment.updateEmployeeDetails;
  personalUpdateUrl = environment.updatePersonalDetails;
  confidentialUpdateUrl = environment.updateConfidentialDetails;
  skillsUpdateUrl = environment.updateSkills;
  socialUpdateUrl = environment.updateSocialDetails;

  userDetails: any = {};
  showAddSkill: boolean = false;
  selectedskillOrCert: any;
  selectedTitlesDropdown: any;
  @ViewChild('mdSlideUp') public mdSlideUp: ModalDirective;
  @ViewChild('mdSlideUpAdd') public mdSlideUpAdd: ModalDirective;
  @ViewChild('smSlideUpAdd') public smSlideUpAdd: ModalDirective;
  @ViewChild('smSlideUp') smSlideUp: ModalDirective;
  selectedColor = 'miracle';
  public uploader: FileUploader = new FileUploader({ url: 'http://localhost:4200', itemAlias: 'photo' });
  
  selectedSideMenu = 0;
  slideUp: any = {
    type: "md"
  }
  slideUpAdd: any = {
    type: "md"
  }

  sideMenu = [
    {
      name: 'Employment',
      icon: 'fa fa-suitcase'
    },


    {
      name: 'Personal',
      icon: 'fa fa-user'
    },
    {
      name: 'Confidential',
      icon: 'fa fa-university'
    },
    {
      name: 'Social',
      icon: 'fa fa-facebook'
    },
    {
     // name: 'Skills & Certifications',
     name: 'Skills',
      icon: 'fa fa-book'
    }

  ]

  skillOrCert = [

   // { value: 'certification', label: 'Certification' },
    { value: 'skill', label: 'Skill' }
  ];
  selectedMultipleOption =[]
  searchTags = [
    { value: 'HTML', label: 'HTML' },
    { value: 'CSS', label: 'CSS' },
    { value: 'Bootstrap', label: 'Bootstrap' },
    { value: 'Datapower', label: 'Datapower' },
    { value: 'Javascript', label: 'Javascript' }
    ];

  selectedOption;
  options = [
    { value: 'jack', label: 'Jacks' },
    { value: 'lucy', label: 'Lucy' },
    { value: 'disabled', label: 'Disabled', disabled: true }
  ];

  notificationModel: any = {
    heading: "Simple Alert",
    desc: "Awesome Loading Circle Animation",
    position: "bottom-right",
    type: "simple"
  }
  selectedTitles: any = 'Employment';

  modelTitles = [
    { value: 'employment', label: 'Employment', select: true },
    { value: 'personal', label: 'Personal' },
    { value: 'confidential', label: 'Confidential' },
    { value: 'social', label: 'Social' },
    { value: 'skills', label: 'Skills' },
    //{ value: 'certifications', label: 'Certifications' },
  ];
  editableUserDetails = new FormGroup({
    AadharName: new FormControl(""),
    AadharNum: new FormControl(""),
    AboutMe: new FormControl(""),
    AccNum: new FormControl(""),
    AliasName: new FormControl(""),
    AlterPhoneNo: new FormControl(""),
    AnniversaryDate: new FormControl(""),
    BankName: new FormControl(""),
    BirthDate: new FormControl(""),
    Country: new FormControl(""),
    CoverPic: new FormControl(""),
    CurStatus: new FormControl(""),
    CellPhoneNo: new FormControl(""),
    DepartmentId: new FormControl(""),
    Email: new FormControl(""),
    Email1: new FormControl(""),
    Email2: new FormControl(""),
    EmpNo: new FormControl(""),
    EmpId: new FormControl(),
    EmployeeTypeId: new FormControl(""),
    FName: new FormControl(""),
    Gender: new FormControl(""),
    HireDate: new FormControl(""),
    HomeAddressId: new FormControl(),
    HomePhoneNo: new FormControl(""),
    IfscCode: new FormControl(""),
    IsManager: new FormControl(),
    IsOperationContactTeam: new FormControl(),
    IsTeamLead: new FormControl(),
    LoginId: new FormControl(""),
    Instagram: new FormControl(""),
    LName: new FormControl(""),
    Location: new FormControl(""),
    Facebook: new FormControl(""),
    Itgbatch: new FormControl(""),
    Twitter: new FormControl(""),
    LinkedIn: new FormControl(""),
    MName: new FormControl(""),
    MaritalStatus: new FormControl(""),
    NameAsPerAcc: new FormControl(""),
    NameAsPerPan: new FormControl(""),
    OpsContactId: new FormControl(""),
    OrgId: new FormControl(""),
    OtherAddressId: new FormControl(),
    PAN: new FormControl(),
    PFNo: new FormControl(""),
    PassportExpiryDate: new FormControl(""),
    PassportNo: new FormControl(""),
    PracticeId: new FormControl(""),
    ProfilePic: new FormControl(""),
    ReportingHr: new FormControl(""),
    ReportsTo: new FormControl(""),
    SkillSet: new FormControl([]),
    State: new FormControl(""),
    SubPractice: new FormControl(""),
    TitleTypeId: new FormControl(""),
    UANNo: new FormControl(""),
    WorkPhoneNo: new FormControl("")
  })
  fileToUpload: File;
  lengthCheck: any = 14;
  eleven: boolean;
  plus: boolean;
  default: boolean;

  constructor(private _notification: MessageService, private route: ActivatedRoute, private searchapi: RestApiService, public navCtrl: NgxNavigationWithDataComponent, private router: Router) {

  }

  changeskillOrCert() {

    if (this.selectedskillOrCert.label == 'Skill') {
      this.showAddSkill = true;
    }
    else
      this.showAddSkill = false;
  }

  changeCategory() {
    this.selectedTitles = this.selectedTitlesDropdown.label;
  }
  createBasicNotification() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Success!!, Sent for approval to partnerships team';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 5000
        });
    }, 1500);
  }

  createNotificationForEdit(selectedTitle) {

    Object.keys(this.editableUserDetails).map((morph)=>{
      if(this.editableUserDetails[morph]=== 'N/A'){
        this.editableUserDetails[morph] = "";
      }
    });
    switch (selectedTitle) {
      case 'Employment':
        //Make Employment API Call

        this.searchapi.postApi(this.employeeUpdateUrl, { "empId": this.userDetails.EmpId.toString(),
         "workPhoneNo": this.editableUserDetails.value.WorkPhoneNo,
          "cellPhoneNo": this.editableUserDetails.value.CellPhoneNo,
        "modifiedBy": JSON.parse(localStorage.getItem('userDetails')).LoginId })
          .subscribe((data) => {

            
            if(this.editableUserDetails.value.WorkPhoneNo === ""){
              this.editableUserDetails.value.WorkPhoneNo = "N/A";
            }
            if(this.editableUserDetails.value.CellPhoneNo === ""){
              this.editableUserDetails.value.CellPhoneNo = "N/A";
            }

            this.userDetails.WorkPhoneNo = this.editableUserDetails.value.WorkPhoneNo;
            this.userDetails.CellPhoneNo = this.editableUserDetails.value.CellPhoneNo;
            this.notificationAlert()
          })
        break;

      case 'Personal':
        //Make Personal API Call
        this.searchapi.postApi(this.personalUpdateUrl, {
          "empId": this.userDetails.EmpId,
          "alterPhoneNo": this.editableUserDetails.value.AlterPhoneNo,
          "aliasName": this.editableUserDetails.value.AliasName,
          "email2": this.editableUserDetails.value.Email2,
          "maritalStatus": this.editableUserDetails.value.MaritalStatus,
          "aboutMe": this.editableUserDetails.value.AboutMe,
          "modifiedBy": JSON.parse(localStorage.getItem('userDetails')).LoginId
        })
          .subscribe((data) => {

            
            if(this.editableUserDetails.value.AlterPhoneNo === ""){
              this.editableUserDetails.value.AlterPhoneNo = "N/A";
            }
            if(this.editableUserDetails.value.AliasName === ""){
              this.editableUserDetails.value.AliasName = "N/A";
            }
            if(this.editableUserDetails.value.Email2 === ""){
              this.editableUserDetails.value.Email2 = "N/A";
            }
            if(this.editableUserDetails.value.MaritalStatus === ""){
              this.editableUserDetails.value.MaritalStatus = "N/A";
            }
            if(this.editableUserDetails.value.AboutMe === ""){
              this.editableUserDetails.value.AboutMe = "N/A";
            }

            this.userDetails.AlterPhoneNo = this.editableUserDetails.value.AlterPhoneNo;
            this.userDetails.AliasName = this.editableUserDetails.value.AliasName;
            this.userDetails.Email2 = this.editableUserDetails.value.Email2;
            this.userDetails.MaritalStatus = this.editableUserDetails.value.MaritalStatus;
            this.userDetails.AboutMe = this.editableUserDetails.value.AboutMe;

            this.notificationAlert()
          })
        break;

      case 'Social':
        //Make Social API Call
        this.searchapi.postApi(this.socialUpdateUrl, {
          "linkedIn":this.editableUserDetails.value.LinkedIn,
          "twitter":this.editableUserDetails.value.Twitter,
         "facebook":this.editableUserDetails.value.Facebook,
         "instagram":this.editableUserDetails.value.Instagram,
         "empId": this.userDetails.EmpId,
         "modifiedBy": JSON.parse(localStorage.getItem('userDetails')).LoginId
     }).subscribe((data) => { 

      
      if(this.editableUserDetails.value.LinkedIn === ""){
        this.editableUserDetails.value.LinkedIn = "N/A";
      }
      if(this.editableUserDetails.value.Facebook === ""){
        this.editableUserDetails.value.Facebook = "N/A";
      }
      if(this.editableUserDetails.value.Twitter === ""){
        this.editableUserDetails.value.Twitter = "N/A";
      }
      if(this.editableUserDetails.value.Instagram === ""){
        this.editableUserDetails.value.Instagram = "N/A";
      }
      
      this.userDetails.LinkedIn = this.editableUserDetails.value.LinkedIn;
      this.userDetails.Twitter = this.editableUserDetails.value.Twitter;
      this.userDetails.Facebook = this.editableUserDetails.value.Facebook;
      this.userDetails.Instagram = this.editableUserDetails.value.Instagram;

      this.notificationAlert()
     })
        break;

      case 'Confidential':
        //Make Confidential API call

        this.searchapi.postApi(this.confidentialUpdateUrl, {
          "empId": this.userDetails.EmpId,
          "bankName": this.editableUserDetails.value.BankName,
          "accNum": this.editableUserDetails.value.AccNum,
          "nameAsPerAcc": this.editableUserDetails.value.NameAsPerAcc,
          "ifscCode": this.editableUserDetails.value.IfscCode,
          "aadharNum": this.editableUserDetails.value.AadharNum,
          "aadharName": this.editableUserDetails.value.AadharName,
          "nameAsPerPan": this.editableUserDetails.value.NameAsPerPan,
          "uanNo": this.editableUserDetails.value.UANNo,
          "pfno": this.editableUserDetails.value.PFNo,
          "ssn": this.editableUserDetails.value.PAN,
          "modifiedBy": JSON.parse(localStorage.getItem('userDetails')).LoginId
        })
          .subscribe((data) => {

            
            if(this.editableUserDetails.value.AccNum === ""){
              this.editableUserDetails.value.AccNum= "N/A";
            }
            if(this.editableUserDetails.value.BankName === ""){
              this.editableUserDetails.value.BankName = "N/A";
            }
            if(this.editableUserDetails.value.NameAsPerAcc === ""){
              this.editableUserDetails.value.NameAsPerAcc = "N/A";
            }
            if(this.editableUserDetails.value.IfscCode === ""){
              this.editableUserDetails.value.IfscCode = "N/A";
            }if(this.editableUserDetails.value.AadharNum === ""){
              this.editableUserDetails.value.AadharNum = "N/A";
            }
            if(this.editableUserDetails.value.AadharName === ""){
              this.editableUserDetails.value.AadharName = "N/A";
            }
            if(this.editableUserDetails.value.NameAsPerPan === ""){
              this.editableUserDetails.value.NameAsPerPan = "N/A";
            }
            if(this.editableUserDetails.value.UANNo === ""){
              this.editableUserDetails.value.UANNo = "N/A";
            }if(this.editableUserDetails.value.PFNo === ""){
              this.editableUserDetails.value.PFNo = "N/A";
            }
            if(this.editableUserDetails.value.PAN === ""){
              this.editableUserDetails.value.PAN = "N/A";
            }
            
            this.userDetails.BankName = this.editableUserDetails.value.BankName;
            this.userDetails.AccNum = this.editableUserDetails.value.AccNum;
            this.userDetails.NameAsPerAcc = this.editableUserDetails.value.NameAsPerAcc;
            this.userDetails.IfscCode = this.editableUserDetails.value.IfscCode;
            this.userDetails.AadharNum = this.editableUserDetails.value.AadharNum;
            this.userDetails.AadharName = this.editableUserDetails.value.AadharName;
            this.userDetails.NameAsPerPan = this.editableUserDetails.value.NameAsPerPan;
            this.userDetails.UANNo = this.editableUserDetails.value.UANNo;
            this.userDetails.PFNo = this.editableUserDetails.value.PFNo;
            this.userDetails.PAN = this.editableUserDetails.value.PAN;

            this.notificationAlert()
          })
          
        break;

      case ('Skills'):
        //Make Skills and Certifications API Call
        this.userDetails.SkillSet = this.editableUserDetails.value.SkillSet;
        this.searchapi.postApi(this.skillsUpdateUrl, {"empId": this.userDetails.EmpId,
         "skills": this.editableUserDetails.value.SkillSet,
        "modifiedBy": JSON.parse(localStorage.getItem('userDetails')).LoginId})
        .subscribe((data) => { 

        })
        break;

      case ('Certifications'):
          //Make Skills and Certifications API Call
          // this.searchapi.postApi(this.skillsCertificationUpdateUrl, {}).subscribe(() => { })
         break;

    }

  }

  notificationAlert() {
    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Information saved succesfully!!';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });
  }



  getSkillOrCert(data) {

    

  }

  showSlideUp(data) {
    this.selectedTitles = data;

    //this.selectedTitlesDropdown.label = this.selectedTitles;
    switch (data){
      case 'Employment':{
        this.selectedTitlesDropdown = this.modelTitles[0];

        break;
      }
      case 'Personal':{
        this.selectedTitlesDropdown = this.modelTitles[1];
        break;
      }
      case 'Confidential':{
        this.selectedTitlesDropdown = this.modelTitles[2];

        break;
      }
      case 'Social':{
        this.selectedTitlesDropdown = this.modelTitles[3];

        break;
      }
      case ('Skills'||'Certifications'):{
        this.selectedTitlesDropdown = this.modelTitles[4];

        break;
      }
    }
    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUp.show();
        break;
      }
      case "lg": {
        this.mdSlideUp.show();
        break;
      }
      case "sm": {
        this.smSlideUp.show();
        break;
      }
    }
  }

  showSlideUpAdd() {

    switch (this.slideUpAdd.type) {
      case "md": {
        this.mdSlideUpAdd.show();
        break;
      }
      case "lg": {
        this.mdSlideUpAdd.show();
        break;
      }
      case "sm": {
        this.smSlideUpAdd.show();
        break;
      }
    }
  }

  ngOnInit() {

    
        this.searchapi.postApi(this.searchUrl, {'empId': JSON.parse(localStorage.getItem('userDetails')).EmpId})
        .subscribe((list) => {

          this.userDetails = list;

          this.userDetails.SkillSet = this.userDetails.SkillSet.split(/[@]+/);
          if ((this.userDetails.SkillSet.length == 1) && (this.userDetails.SkillSet[0] == "" || " ")) {
            this.userDetails.SkillSet.pop();
          }
          let g = [];
          let correctedUserData = [];

          this.userDetails.SkillSet.map((k)=>{
            k = k.trim();
            correctedUserData.push(k);
            g.push({value: k, label: k})
          })

          this.userDetails.SkillSet = correctedUserData;

          this.searchTags = this.searchTags.concat(g)
          this.merge_array(this.searchTags).then((data)=>{

            this.searchTags = data;
          })


          this.userDetails.Email = "mailto:" + this.userDetails.Email1;
          Object.keys(this.userDetails).map((d)=>{

            if((this.userDetails[d] === null) || (this.userDetails[d] ==="")){
              this.userDetails[d] = 'N/A';
            }
          })

          this.editableUserDetails.setValue(this.userDetails);
          this.onKey(this.editableUserDetails.value.WorkPhoneNo, 'VoIP')
        })

    //Below method subscribe the recent data on change of value, works only for Reactive Form Values
    this.editableUserDetails.get('SkillSet').valueChanges.subscribe(data => {
      
      //Not efficient way to change validation, change when time permits
      if(this.editableUserDetails.get('SkillSet').untouched && (this.editableUserDetails.get('SkillSet').value.length != this.userDetails.SkillSet.length)){
        
        //set the field as touched on clearing a skill from skillset
        this.editableUserDetails.get('SkillSet').markAsTouched({onlySelf: true});
       
      }
    })

    }

    async merge_array(array) {
      let uniqueTestArray = [];
      let uniqueArray = [];

      let len = array.length;
        // Loop through array values
        for(let value of array){
          if(uniqueTestArray.indexOf(value.label) === -1) {
              uniqueTestArray.push(value.label);
              uniqueArray.push(value);
          }
        }
        return await uniqueArray;
    }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }
  clearNavCtrlId(){
    this.navCtrl.navigate('dashboard/view-profile', {'id': undefined});
  }
  test(e){

  }
  onKey(value, toBeUpdated){

    switch(toBeUpdated){
      case 'Alternate': {
        if(value.charAt(0) != ""){
        this.editableUserDetails.controls['AlterPhoneNo'].patchValue(new AsYouType('US').input(value).replace(/\ /g,"-"));
        this.editableUserDetails.controls['AlterPhoneNo'].updateValueAndValidity();
        }
        break;
      }
      case 'VoIP': {

        switch(value.charAt(0)){
          case "1":{
            this.eleven = true;
            this.plus = false;
            this.default = false;
            this.editableUserDetails.controls['WorkPhoneNo'].setValidators(Validators.minLength(16));
            this.editableUserDetails.controls['WorkPhoneNo'].patchValue(new AsYouType('US').input(value).replace(/\ /g,"-"));
            this.editableUserDetails.controls['WorkPhoneNo'].updateValueAndValidity();
            break;
          }
          case "+":{
            this.plus = true;
            this.eleven = false;
            this.default = false;
            this.editableUserDetails.controls['WorkPhoneNo'].setValidators(Validators.minLength(15));
            this.editableUserDetails.controls['WorkPhoneNo'].patchValue(new AsYouType('US').input(value).replace(/\ /g,"-"));
            this.editableUserDetails.controls['WorkPhoneNo'].updateValueAndValidity();
            break;
          }
          case "":{
            break;
          }
          default :{
            this.default = true;
            this.eleven = false;
            this.plus = false;
            this.editableUserDetails.controls['WorkPhoneNo'].setValidators(Validators.minLength(14));
            this.editableUserDetails.controls['WorkPhoneNo'].patchValue(new AsYouType('US').input(value).replace(/\ /g,"-"));
            this.editableUserDetails.controls['WorkPhoneNo'].updateValueAndValidity();
            break;
          }
        }
        break;
      }
      case 'Home': {
        if(value.charAt(0) != ""){
          this.editableUserDetails.controls['CellPhoneNo'].patchValue(new AsYouType('US').input(value).replace(/\ /g,"-"));
          this.editableUserDetails.controls['CellPhoneNo'].updateValueAndValidity();
        }
        break;
      }
    }
  }
  ngOnDestroy(){
    localStorage.removeItem('navCtrlId');
  }
}
