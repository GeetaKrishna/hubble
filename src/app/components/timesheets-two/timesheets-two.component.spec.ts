import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesheetsTwoComponent } from './timesheets-two.component';

describe('TimesheetsTwoComponent', () => {
  let component: TimesheetsTwoComponent;
  let fixture: ComponentFixture<TimesheetsTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesheetsTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesheetsTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
