import { Component, OnInit,ViewChild,ViewEncapsulation  } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-timesheets-two',
  templateUrl: './timesheets-two.component.html',
  styleUrls: ['./timesheets-two.component.scss']
})
export class TimesheetsTwoComponent implements OnInit {
  endValue = '05/05/19'
  subValue= '05/10/19'

  editTimeSheet: any;
  @ViewChild('mdSlideUpDelete') public mdSlideUpDelete: ModalDirective;
 
  
  
  options = [
    {value:'a', label:'All'},
    { value: 'jack', label: 'Approved' },
    { value: 'lucy', label: 'Submitted' },
    { value: 'lucy', label: 'Rejected' },
  ];

  timesheetHistory = [
    {
      startDate:'02/05/17',
      endDate:'09/05/17',
      billable:40,
      others:0,
      submittedDate:'08/05/17',
      status:'Submitted',
      approvedDate:'12/05/17',
    },
    {
      startDate:'02/05/17',
      endDate:'09/05/17',
      billable:40,
      others:0,
      submittedDate:'08/05/17',
      status:'Submitted',
      approvedDate:'12/05/17',
    },
    {
      startDate:'02/05/17',
      endDate:'09/05/17',
      billable:32,
      others:8,
      submittedDate:'08/05/17',
      status:'Approved',
      approvedDate:'12/05/17',
    },
    {
      startDate:'02/05/17',
      endDate:'09/05/17',
      billable:40,
      others:0,
      submittedDate:'08/05/17',
      status:'Submitted',
      approvedDate:'12/05/17',
    },
    {
      startDate:'02/05/17',
      endDate:'09/05/17',
      billable:40,
      others:0,
      submittedDate:'08/05/17',
      status:'Rejected',
      approvedDate:'12/05/17',
    },
  ]
  slideUp:any = {
    type:"sm"
  }
  
  showTimeSheetEdit(){
   this.editTimeSheet=!this.editTimeSheet;
  }
  showSlideUpDelete(){
    
    switch(this.slideUp.type) { 
      case "sm": { 
        this.mdSlideUpDelete.show();
         break; 
      } 
   
     
   }     
  }

  getStatus(data){
   
      switch (data) {
        case 'Submitted':
          return '#00aae7';
        case 'Rejected':
          return '#f35958';
        case 'Approved':
          return '1dbb99';
      }
    
  }
  constructor() { }

  ngOnInit() {
  }

}
