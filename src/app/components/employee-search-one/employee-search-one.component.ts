import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-search-one',
  templateUrl: './employee-search-one.component.html',
  styleUrls: ['./employee-search-one.component.scss']
})
export class EmployeeSearchOneComponent implements OnInit {
  selectedOption;
  department = [
  { value: 'marketing', label: 'Marketing' },
  { value: 'development', label: 'Development' },
  { value: 'sales', label: 'Sales' },
  { value: 'recruitment', label: 'Recruitment' },
  ];

  location = [
    { value: 'miracle city', label: 'Miracle City' },
    { value: 'miracle heights', label: 'Miracle Heights' },
    { value: 'headquarters', label: 'Headquarters' },
    { value: 'miracle valley', label: 'Miracle Valley' },
    ];

    designation =  [
      { value: 'lead', label: 'Lead' },
      { value: 'manager', label: 'Manager' },
      { value: 'director', label: 'Director' },
      { value: 'vp', label: 'Pre-sales' },
      ];  
  constructor() { }

  ngOnInit() {
  }

}
