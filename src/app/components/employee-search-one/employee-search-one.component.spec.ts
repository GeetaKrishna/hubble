import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeSearchOneComponent } from './employee-search-one.component';

describe('EmployeeSearchOneComponent', () => {
  let component: EmployeeSearchOneComponent;
  let fixture: ComponentFixture<EmployeeSearchOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeSearchOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeSearchOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
