import { Component, OnInit, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { RestApiService } from '../../services/rest-api.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.scss']
})
export class EmployeeSearchComponent implements OnInit {
  selectedOption;
  searchData: any = "";
  departmentData: any = "";
  locationData: any = "";
  titleData: any = "";
  department = [];

  location = [];

  designation = [];
  items: any;

  contactList = [];
  empSearch: any = environment.employeeSearch;
  departmentURL: any = environment.departmentsURL;
  departmentsList: any;

  data = {
    "searchKey": '',
    "pageNum": 1
  }
  load: boolean = false;
  TotalCount: any;
  count: any = 1;
  constructor(private loginapi: RestApiService, public navCtrl: NgxNavigationWithDataComponent) {


    this.loginapi.postApi(this.empSearch, this.data)
      .subscribe((list) => {
        this.TotalCount = list[list.length - 1];
        list.pop();
        console.log(list, 'list from API');
        this.contactList = list;
        this.items = list;
      })

    this.loginapi.getApi(this.departmentURL).subscribe((departmentsList) => {
      this.department = departmentsList[0].Departments;

      for (let value of departmentsList[2].Locations) {
        if (value.label != null) {
          this.location.push(value)
        }
      }

      this.designation = departmentsList[1].Designations;
    })
  }

  ngOnInit() {
  }
  navigateToABout(data) {
    console.log(data, 'data from navigation')
    this.navCtrl.navigate('dashboard/view-profile', data);
  }
  fileNameLimit(fileName) {

    if (fileName.length >= 21) {
      return fileName.substring(0, 19) + '...';
    } else {
      return fileName;
    }
  }

  search() {
    this.load = false;
    this.count = 1;

    console.log("location", this.locationData)
    if (this.locationData == null) {
      this.locationData = {
        "value": ''
      }
    }
    if (this.departmentData == null) {
      this.departmentData = {
        "value": ''
      }
    }
    if (this.titleData == null) {
      this.titleData = {
        "value": ''
      }
    }

    let data = {
      "searchKey": this.searchData || '',
      "departmentId": this.departmentData.value || '',
      "titleTypeId": this.titleData.value || '',
      "location": this.locationData.value || '',
      "pageNum": 1
    }
    this.loginapi.postApi(this.empSearch, data).subscribe((list) => {
      this.TotalCount = list[list.length - 1];
      console.log(this.TotalCount, 'totalCount')
      list.pop()
      this.contactList = list;
      this.items = list;
    })
  }

  test(r){
    console.log(r);
  }
  // 
  fetchMore() {
    this.count++;
    this.fetchNextChunk(this.contactList.length, 10, this.count).then(chunk => {
      this.contactList = this.contactList.concat(chunk);
      this.items = this.items.concat(chunk);
      this.load = false;
    }, () => this.load = false);
  }
// 
  showEnd(event) {
    if (event.endIndex !== this.contactList.length - 1) return;
    console.log(event, this.contactList.length - 1, 'event at end of navigation after reaching end')
   // this.load = true;
    //this.fetchMore();
    if (this.TotalCount) {
      if (this.TotalCount.count === this.items.length) {
        this.load = false;
      }
      else {
        this.load = true;
        //this.fetchMore();
      }
    }

  }

  protected fetchNextChunk(skip: number, limit: number, counter: number) {
    return new Promise((resolve, reject) => {
      let data = {
        "searchKey": this.searchData || '',
        "departmentId": this.departmentData.value || '',
        "titleTypeId": this.titleData.value || '',
        "location": this.locationData.value || '',
        "pageNum": counter
      }
      this.loginapi.postApi(this.empSearch, data).subscribe((list) => {
        list.pop();
        resolve(list)
      })
    });
  }
}
