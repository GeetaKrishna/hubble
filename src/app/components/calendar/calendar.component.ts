import { Component, OnInit, ViewChild } from '@angular/core';
import { OptionsInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { FullCalendarComponent } from '@fullcalendar/angular';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  
  styleUrls: [
    './calendar.component.scss'

  ]
})
export class CalendarComponent implements OnInit {
  year = [
    {
      value:'2019',label:'2019'
    },
    {
      value:'2018',label:'2018'
    },
  ]
  holidays = [
    {
      title:'Diwali',
      date:'07/11/2019'
    },
    {
      title:'Eid',
      date:'01/08/2019'
    },
    {
      title:'Sankranthi',
      date:'07/01/2019'
    },
    {
      title:'Christmas',
      date:'12/25/2019'
    },
  ]
  birthdays = [
    {
      title:'Rajeev Varma',
      date:'02/08/2019'
    },
    {
      title:'Deepika',
      date:'08/08/2019'
    },
   
  ]
  eventsList = [
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    {
      title:'ASUG Chapter Meeting',
      date:'02/11/19'
    },
    
  ]
  options: OptionsInput;
  eventsModel: any;
  @ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;
  tooltipData: any;



  ngOnInit() {
    this.options = {
      editable: true,
      customButtons: {
        myCustomButton: {
          text: 'custom!',
          click: function () {
            alert('clicked the custom button!');
          }
        }
      },
      header: {
        left: 'prev,next  myCustomButton',
        center: 'title',
        right: 'today'
      },
      plugins: [dayGridPlugin, interactionPlugin]
    };


    this.updateEvents();



  }
  eventClick(model) {
    console.log(model);
  }
  mouseOver(data){
    console.log(data.event.title);
    this.tooltipData = data.event.title;
  }

  eventrender(event, element)
  {
    
     event.element[0].querySelectorAll(".fc-content")[0].setAttribute("data-tooltip", event.event.title);
  }


  eventDragStop(model) {
    console.log(model);
  }
  dateClick(model) {
    console.log(model);
  }
  updateHeader() {
    this.options.header = {
      left: 'prev,next myCustomButton',
      center: 'title',
      right: ''
    };
  }
  updateEvents() {
    this.eventsModel = [
      {
        title: 'GSM June,2019',
        // start: '2019-06-10 15:45:17',
       
        // end: '2019-06-12 15:59:17',
        start: '2019-06-15',
       
        end: '2019-06-15',
        color: '#00aae7', 
       textColor: 'white'
      
     
      },
      {
        title: 'ASUG June,2019',
        start: '2019-06-15',
       
        end: '2019-06-15',
        color: '#ef4048', 
      textColor: 'white'
      
     
      },
      {
        title: 'SAP Saphire,2019',
        start: '2019-06-15',
       
        end: '2019-06-15',
        color: '#2368a0', 
      textColor: 'white'
      
     
      },
      {
        title: 'IBM,2019',
        start: '2019-06-15',
       
        end: '2019-06-15',
        color: '#00aae7', 
      textColor: 'white'
      
     
      },
      {
        title: 'GSM June,2019',
        start: '2019-06-15',
       
        end: '2019-06-15',
        color: '#00aae7', 
      textColor: 'white'
      
     
      },
     
      {
        title: 'ASUG Minnessotta Event',
        start: '2019-06-10',
       
        end: '2019-06-12',
        color: '#00aae7', 
      textColor: 'white',
      timeFormat: 'h(:mm)a'
      
     
      },
    
  ];
  }
  get yearMonth(): string {
    const dateObj = new Date();
    return dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
  }

  eventRender() {
    console.log('rajiv');
    

}

}