import { Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MessageService } from '../../@pages/components/message/message.service';
@Component({
  selector: 'app-bms',
  templateUrl: './bms.component.html',
  styleUrls: ['./bms.component.scss']
})
export class BmsComponent implements OnInit {
  @ViewChild('mdSlideUpShowBridge') public mdSlideUpShowBridge: ModalDirective;
  @ViewChild('mdSlideUpDelete') public mdSlideUpDelete: ModalDirective;
  @ViewChild('mdSlideUpAddBridge') public mdSlideUpAddBridge: ModalDirective;
  selectedColor = 'miracle';
  notificationModel: any =    {
    heading:"Simple Alert",
    desc:"Awesome Loading Circle Animation",
    position:"bottom-right",
    type:"simple"
  }
  options = [
    { value: 'a', label: 'EST' },
  ];

  duration = [
    { value: 'a', label: '1' },
    { value: 'a', label: '2' },
    { value: 'a', label: '3' },
    
  ]
  meridiem = [
    { value: 'a', label: 'AM' },
    { value: 'b', label: 'PM' },
  ]
  bridgeHistory = [
    {
      sl:'1',
      extension:'B0802',
      number:'+12484120802',
      passcode:'786789',
      time:'11 AM - 12 AM',
      date:'02/05/18',
      by:'rgottimukkala1'
    },
    {
      sl:'2',
      extension:'B0812',
      number:'+12484120812',
      passcode:'786487',
      time:'09 AM - 10 AM',
      date:'05/07/18',      
      by:'slekala'
    },
  ]
  
  tags = ['spatnala'];
  tags1 = [];
  _date = null;
  _date_time = null;

  slideUp: any = {
    type: "md"
  }
  constructor(private _notification: MessageService) { }
  
  showBridgeDetails(){
   
    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUpShowBridge.show();
        break;
      }


    }
  }

  showDelete(){
    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUpDelete.show();
        break;
      }


    }
  }
  addBridge(){
    this.mdSlideUpAddBridge.show();
  }

  createAddBridgeNotification() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Bridge Details Added Successfully!!';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });

  }

  createCopiedDetailsNotification() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Bridge Details Copied Successfully!!';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });

  }

  createUpdateBridgeNotification() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Bridge Details Updated Successfully!!';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });

  }
  createCancelBridgeNotification() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Bridge Cancelled Successfully!!';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });

  }
  ngOnInit() {
  }

}
