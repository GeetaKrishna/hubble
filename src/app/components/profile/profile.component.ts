import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('mdSlideUp') public mdSlideUp: ModalDirective;
  @ViewChild('smSlideUp') smSlideUp: ModalDirective;

  slideUp: any = {
    type: "md"
  }
  constructor() { 

  }

  ngOnInit() {
  }

  showSlideUp() {

    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUp.show();
        break;
      }
      case "lg": {
        this.mdSlideUp.show();
        break;
      }
      case "sm": {
        this.smSlideUp.show();
        break;
      }
    }
  }

}
