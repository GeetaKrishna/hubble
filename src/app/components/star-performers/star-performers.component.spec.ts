import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarPerformersComponent } from './star-performers.component';

describe('StarPerformersComponent', () => {
  let component: StarPerformersComponent;
  let fixture: ComponentFixture<StarPerformersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarPerformersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarPerformersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
