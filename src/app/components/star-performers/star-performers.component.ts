import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-star-performers',
  templateUrl: './star-performers.component.html',
  styleUrls: ['./star-performers.component.scss']
})
export class StarPerformersComponent implements OnInit {
@ViewChild('addSurveyModal') public addSurveyModal: ModalDirective;

  years = [
    '2019','2018','2017','2016','2015'
  ];

  months = [
    'January','February','March','April','May','June','July','August'
  ];
  starPerformerList = [
    {
      month:'January',year:'2019',status:'Initiated'
    },
    {
      month:'February',year:'2019',status:'Nominated'
    },
    {
      month:'March',year:'2019',status:'Submitted'
    },
    {
      month:'April',year:'2019',status:'Approved'
    },
    {
      month:'May',year:'2019',status:'Published'
    }
  ];
  constructor() { }
  getStatus(data){
    switch (data) {
      case 'Applied':
        return '#00aae7';
      case 'Rejected':
        return '#f35958';
      case 'Approved':
        return '1dbb99';
    }
  }

  showSurvey() {
  
        this.addSurveyModal.show();
      
 
  }


  ngOnInit() {
  }

}
