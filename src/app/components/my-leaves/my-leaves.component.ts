import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalDirective, BsModalService } from 'ngx-bootstrap/modal';
import { RestApiService } from '../../services/rest-api.service';
import { environment } from '../../../environments/environment';

import * as moment from 'moment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from '../../@pages/components/message/message.service';
@Component({
  selector: 'app-my-leaves',
  templateUrl: './my-leaves.component.html',
  styleUrls: ['./my-leaves.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MyLeavesComponent implements OnInit {
  selectedOption: any = {
    value: ''
  };

  cancelButton: boolean;
  updateButton: boolean;

  duplicateShowLeaveEditData: any = new FormGroup({
    Id: new FormControl(),
    StartDate: new FormControl(''),
    EndDate: new FormControl(''),
    Reason: new FormControl(''),
    ElementNumber: new FormControl(''),
    LeaveType: new FormControl(),
    startTime: new FormControl(),
    endTime: new FormControl(),
    category: new FormControl(),
    otherCategory: new FormControl(),
    includeWeekend: new FormControl()
  });

  notificationModel: any = {
    heading: "Simple Alert",
    desc: "Awesome Loading Circle Animation",
    position: "bottom-right",
    type: "simple"
  }

  selectedColor = 'miracle';
  availableLeaves: any;

  constructor(private modalService: BsModalService, private searchapi: RestApiService, private _notification: MessageService) {

  }
  @ViewChild('mdSlideUpDelete') public mdSlideUpDelete: ModalDirective;
  @ViewChild('mdSlideUpEditLeave') public mdSlideUpEditLeave: ModalDirective;
  @ViewChild('mdSlideUpAddLeave') public mdSlideUpAddLeave: ModalDirective;
  searchUrl = environment.searchLeavesOfEmployee;
  applyLeaveUrl = environment.applyLeaves;
  updateUrl = environment.updateLeaves;
  cancelLeaveUrl = environment.cancelLeave;
  getLeavesUrl = environment.availableLeaves;

  me: boolean = true;
  opacity;
  startValue;
  selectedIncludeWeekends: any;
  //Date Range Setup
  _startDate = null;
  _endDate = null;

  _newStartDate: any = null;
  _newEndDate: any = null;
  newArray = (len) => {
    const result = [];
    for (let i = 0; i < len; i++) {
      result.push(i);
    }
    return result;
  };

  //Below methods help to disable dates and set ranges while editing dates of leave
  _startValueChange = (startDateValue) => {
    if (startDateValue > this.duplicateShowLeaveEditData.get('EndDate').value) {
      this.duplicateShowLeaveEditData.patchValue({ 'EndDate': null });
    }
  };

  _endValueChange = (endDateValue) => {
    if (this.duplicateShowLeaveEditData.get('StartDate').value > endDateValue) {
      this.duplicateShowLeaveEditData.patchValue({ 'StartDate': null });
    }
  };

  _disabledStartDate = (startValue) => {
    if (!startValue || !this.duplicateShowLeaveEditData.get('EndDate').value) {
      return false;
    }
    return new Date(startValue).getTime() >= new Date(this.duplicateShowLeaveEditData.get('EndDate').value).getTime();
  };

  _disabledEndDate = (endValue) => {
    if (!endValue || !this.duplicateShowLeaveEditData.get('StartDate').value) {
      return false;
    }
    // 
    return new Date(endValue).getTime() <= new Date(this.duplicateShowLeaveEditData.get('StartDate').value).getTime();
  };

  //Below methods help to disable dates and set ranges while applying leaves
  _startNewValueChange = (startDateValue) => {
    if (startDateValue > this.applyLeaveDetails.get('endDate').value) {
      this.applyLeaveDetails.patchValue({ 'endDate': null });
    }
  };

  _endNewValueChange = (endDateValue) => {
    if (this.applyLeaveDetails.get('startDate').value > endDateValue) {
      this.applyLeaveDetails.patchValue({ 'startDate': null });
    }
  };

  _disabledNewStartDate = (startValue) => {
    if (!startValue || !this.applyLeaveDetails.get('endDate').value) {
      return false;
    }
    return new Date(startValue).getTime() >= new Date(this.applyLeaveDetails.get('endDate').value).getTime();
  };

  _disabledNewEndDate = (endValue) => {
    if (!endValue || !this.applyLeaveDetails.get('startDate').value) {
      return false;
    }
    return new Date(endValue).getTime() <= new Date(this.applyLeaveDetails.get('startDate').value).getTime();
  };

  //for Search Operations 
  _startSearchValueChange = (startDateValue) => {
    if (startDateValue > this._endDate) {
      this._endDate = null;
    }
  };

  _endSearchValueChange = (endDateValue) => {
    if (this._startDate > endDateValue) {
      this._startDate = null;
    }
  };

  _disabledSearchStartDate = (startValue) => {
    if (!startValue || !this._endDate) {
      return false;
    }
    return startValue.getTime() >= this._endDate.getTime();
  };

  _disabledSearchEndDate = (endValue) => {
    if (!endValue || !this._startDate) {
      return false;
    }
    return endValue.getTime() <= this._startDate.getTime();
  };


  showHalfDay: boolean = false;
  showLeaveEditData: any = {
    startDate: new Date(),
    endDate: new Date()
  };
  get _isSameDay() {
    return this._startDate && this._endDate && moment(this._startDate).isSame(this._endDate, 'day')
  }
  get _endTime() {
    return {
      nzHideDisabledOptions: true,
      nzDisabledHours: () => {
        return this._isSameDay ? this.newArray(this._startDate.getHours()) : [];
      },
      nzDisabledMinutes: (h) => {
        if (this._isSameDay && h === this._startDate.getHours()) {
          return this.newArray(this._startDate.getMinutes());
        }
        return [];
      },
      nzDisabledSeconds: (h, m) => {
        if (this._isSameDay && h === this._startDate.getHours() && m === this._startDate.getMinutes()) {
          return this.newArray(this._startDate.getSeconds());
        }
        return [];
      }
    }
  }

  //Pagination number
  p = 1;

  //END Date Range
  showOtherTitle: boolean = false;
  selectedLeaveCategory;
  selectIncludeWeekends;

  //Object to hold the details of New Leave
  applyLeaveDetails = new FormGroup({
    "empId": new FormControl(JSON.parse(localStorage.getItem('userDetails')).EmpId),
    "leaveRequestType": new FormControl("Full Day"),
    "startDate": new FormControl(),
    "endDate": new FormControl(),
    "startTime": new FormControl(""),
    "endTime": new FormControl(""),
    "category": new FormControl(""),
    "includeWeekend": new FormControl("No"),
    "reason": new FormControl(""),
    "otherCategory": new FormControl(""),
    "reportsTo": new FormControl(JSON.parse(localStorage.getItem('userDetails')).ReportsTo),
    "orgId": new FormControl(JSON.parse(localStorage.getItem('userDetails')).OrgId),
    "departmentId": new FormControl(JSON.parse(localStorage.getItem('userDetails')).DepartmentId)
  })

  options = [
    { value: 'All', label: 'All' },
    { value: 'Approved', label: 'Approved' },
    { value: 'Applied', label: 'Applied' },
    //{ value: 'Rejected', label: 'Rejected' },
    { value: 'Cancelled', label: 'Cancelled' },
  ];
  _date_time = null;
  leaveTypes = [
    { label: "Full Day" },
    { label: "Half Day" }
  ]
  applyOptions = [
    {
      value: '', label: 'Post Approval'
    },
    {
      value: '', label: 'Comp-time'
    },
    {
      value: '', label: 'Vacation'
    },
    {
      value: '', label: 'Time-off'
    }

  ]
  LeaveDays = [

    {
      value: '', label: '1'
    },
    {
      value: '', label: '2'
    },
    {
      value: '', label: '3'
    },
    {
      value: '', label: '4'
    },
    {
      value: '', label: '5'
    },
    {
      value: '', label: '6'
    },
    {
      value: '', label: '7'
    },
    {
      value: '', label: '8'
    },
    {
      value: '', label: '9'
    },
  ]

  LeaveCategory = [
    {
      value: '', label: 'Sick Day'
    },
    {
      value: '', label: 'Family Care'
    },
    {
      value: '', label: 'Govt Works'
    },

    {
      value: '', label: 'Civic Duties'
    },
    {
      value: '', label: 'Parental Leave'
    },
    {
      value: '', label: 'Private Matters'
    },
    {
      value: '', label: 'Birthday / Anniversary'
    },

    {
      value: '', label: 'Religious Matter'
    },

    {
      value: '', label: 'Community Events'
    },
    {
      value: '', label: 'Others'
    },
  ]

  LeavesHistory = [];

  selectLeaveType;

  includeWeekends = [
    { value: 'Yes', label: 'Yes' },
    { value: 'No', label: 'No', select: true },
  ];

  slideUp: any = {
    type: "md"
  }

  leaveTypeChange(data, type) {

    if (data == 'Full Day') {

      this.showHalfDay = false;
      if (type === 'new') {
        this.applyLeaveDetails.controls.endDate.setValidators([Validators.required])
        this.applyLeaveDetails.controls.includeWeekend.setValidators([Validators.required]);
        this.applyLeaveDetails.controls.endTime.clearValidators();
        this.applyLeaveDetails.controls.startTime.clearValidators();
        this.applyLeaveDetails.controls.endTime.updateValueAndValidity();
        this.applyLeaveDetails.controls.startTime.updateValueAndValidity();
        this.applyLeaveDetails.controls.endDate.updateValueAndValidity();
        this.applyLeaveDetails.controls.includeWeekend.updateValueAndValidity();
      }
      else if (type === 'edit') {
        this.duplicateShowLeaveEditData.controls.EndDate.setValidators([Validators.required])
        this.duplicateShowLeaveEditData.controls.includeWeekend.setValidators([Validators.required]);
        this.duplicateShowLeaveEditData.controls.endTime.clearValidators();
        this.duplicateShowLeaveEditData.controls.startTime.clearValidators();
        this.duplicateShowLeaveEditData.controls.endTime.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.startTime.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.EndDate.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.includeWeekend.updateValueAndValidity();
      }
    }
    else {
      this.showHalfDay = true;
      if (type === 'edit') {
        this.duplicateShowLeaveEditData.controls.EndDate.clearValidators();
        this.duplicateShowLeaveEditData.controls.includeWeekend.clearValidators();
        this.duplicateShowLeaveEditData.controls.endTime.setValidators([Validators.required, Validators.pattern("[0-9]{2}:[0-9]{2}:[0-9]{2}")]);
        this.duplicateShowLeaveEditData.controls.startTime.setValidators([Validators.required, Validators.pattern("[0-9]{2}:[0-9]{2}:[0-9]{2}")]);
        this.duplicateShowLeaveEditData.controls.endTime.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.startTime.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.EndDate.updateValueAndValidity();
        this.duplicateShowLeaveEditData.controls.includeWeekend.updateValueAndValidity();
      }
      else if (type === 'new') {
        this.applyLeaveDetails.controls.endDate.clearValidators();
        this.applyLeaveDetails.controls.includeWeekend.clearValidators();
        this.applyLeaveDetails.controls.endTime.setValidators([Validators.required, Validators.pattern("[0-9]{2}:[0-9]{2}:[0-9]{2}")]);
        this.applyLeaveDetails.controls.startTime.setValidators([Validators.required, Validators.pattern("[0-9]{2}:[0-9]{2}:[0-9]{2}")]);
        this.applyLeaveDetails.controls.endTime.updateValueAndValidity();
        this.applyLeaveDetails.controls.startTime.updateValueAndValidity();
        this.applyLeaveDetails.controls.endDate.updateValueAndValidity();
        this.applyLeaveDetails.controls.includeWeekend.updateValueAndValidity();
      }
    }
  }

  changeLeaveCategory(data, type) {

    if (data === 'Others') {
      if (type === 'edit') {
        // duplicateShowLeaveEditData.get("otherCategory")
        this.duplicateShowLeaveEditData.controls.otherCategory.setValidators([Validators.required]);
        this.duplicateShowLeaveEditData.controls.otherCategory.updateValueAndValidity();
      }
      else if (type === 'new') {
        this.applyLeaveDetails.controls.otherCategory.setValidators([Validators.required]);
        this.applyLeaveDetails.controls.otherCategory.updateValueAndValidity();
      }

      this.showOtherTitle = true;
    }
    else
      this.showOtherTitle = false;
    if (type === 'edit') {
      this.duplicateShowLeaveEditData.controls.otherCategory.clearValidators();
      this.duplicateShowLeaveEditData.controls.otherCategory.updateValueAndValidity();
    }
    else if (type === 'new') {
      this.applyLeaveDetails.controls.otherCategory.clearValidators();
      this.applyLeaveDetails.controls.otherCategory.updateValueAndValidity();
    }
  }

  showSlideUpDelete() {
    this.opacity = 0;
    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUpDelete.show();
        break;
      }

    }
  }

  showLeaveEdit(data, elementNumber) {
    //console.log(data)
    switch ('md') {
      case "md": {
        //if it doesn't work, use bsModal
        //console.log(data);
        this.duplicateShowLeaveEditData.patchValue({
          'StartDate': data.StartDate,
          'EndDate': data.EndDate,
          'Reason': data.Reason,
          'ElementNumber': elementNumber,
          'LeaveType': data.LeaveRequestType,
          'startTime': data.StartDate.split(" ")[1], 'endTime': data.EndDate.split(" ")[1],
          'category': data.LeaveCategory,
          'includeWeekend': data.IncludeWeekend,
          'Id': data.Id
        })
        //this.modalService.show(MyLeavesComponent, {initialState: this.showLeaveEditData});
        //console.log(this.duplicateShowLeaveEditData)
        if (data.STATUS === 'Applied') {
          //console.log(new Date(data.StartDate.split(" ")[0]) >= new Date(data.AppliedDate.split(" ")[0]));
          if(new Date(data.StartDate.split(" ")[0]) >= new Date(data.AppliedDate.split(" ")[0])){
            this.cancelButton = false;
          }
          else{
            this.cancelButton = true;
          }
          this.updateButton = true;
        }
        else {
          this.cancelButton = false;
          this.updateButton = false;
        }
        this.mdSlideUpEditLeave.show();
        break;
      }
    }
  }

  showAddLeave() {
    switch ('md') {
      case "md": {
        this.mdSlideUpAddLeave.show();
        break;
      }
    }
  }
  getStatus(data) {

    switch (data) {
      case 'Applied':
        return '#00aae7';
      case 'Cancelled':
        return '#f35958';
      case 'Approved':
        return '1dbb99';
    }
  }

  finalNumber() {
    return (((((this.p > 1 ? this.p : 1) - 1) * 10 + 10) > this.LeavesHistory.length) ? (this.LeavesHistory.length) : ((((this.p > 1 ? this.p : 1) - 1) * 10) + 10));
  }
  leaveSearch() {
    //console.log(this._startDate, this.selectedOption, moment(this._startDate).format('YYYY-MM-DD'))

    let _newStartDate = moment(this._startDate).format('YYYY-MM-DD');
    let _newEndDate = moment(this._endDate).format('YYYY-MM-DD');
    if (_newStartDate === "Invalid date") {
      _newStartDate = '';
    }
    if (_newEndDate === "Invalid date") {
      _newEndDate = '';
    }
    let data = {
      "empId": (JSON.parse(localStorage.getItem('userDetails')).EmpId).toString(),
      "startDate": _newStartDate,
      "endDate": _newEndDate,
      "status": (this.selectedOption != null) ? this.selectedOption.value : ''
    }

    this.searchapi.postApi(this.searchUrl, data).subscribe(res => {
      console.log(res, 'response')
      this.LeavesHistory = res;
    }, err => {
      //console.log(err, 'error')
    })
  }

  ngOnInit() {
    this.searchapi.postApi(this.searchUrl, {
      "empId": (JSON.parse(localStorage.getItem('userDetails')).EmpId).toString(),
      "startDate": '',
      "endDate": '',
      "status": ''
    }).subscribe(res => {
      console.log(res, 'responser')
      this.LeavesHistory = res;
    }, err => {
      //console.log(err, 'error')
    })

    this.searchapi.postApi(this.getLeavesUrl, {"empNo": JSON.parse(localStorage.getItem('userDetails')).EmpId})
    .subscribe((leaves)=>{
      this.availableLeaves = leaves.AvialbleLeaves;
    })

    //Below methods subscribe the recent data on change of value, works only for Reactive Form Values
    this.applyLeaveDetails.get('leaveRequestType').valueChanges.subscribe(data => {
      this.leaveTypeChange(data, 'new');
    })

    this.applyLeaveDetails.get('startDate').valueChanges.subscribe(data => {
      this._startNewValueChange(data);
    })

    this.applyLeaveDetails.get('endDate').valueChanges.subscribe(data => {
      if (data != null) {
        this._endNewValueChange(data);
      }
    })

    this.duplicateShowLeaveEditData.get('StartDate').valueChanges.subscribe(data => {
      this._startValueChange(data);
    })

    this.duplicateShowLeaveEditData.get('EndDate').valueChanges.subscribe(data => {
      if (data != null) {
        this._endValueChange(data);
      }
    })

    this.duplicateShowLeaveEditData.get('LeaveType').valueChanges.subscribe(data => {
      this.leaveTypeChange(data, 'edit');
    })

    this.duplicateShowLeaveEditData.get('category').valueChanges.subscribe(data => {
      this.changeLeaveCategory(data, 'edit');
    })

    this.applyLeaveDetails.get('category').valueChanges.subscribe(data => {
      this.changeLeaveCategory(data, 'new');
    })

  }

  //To create  a new Leave
  applyLeave() {

    if (moment(this.applyLeaveDetails.get('startDate').value).format('YYYY-MM-DD') != "Invalid date") {
      this.applyLeaveDetails.patchValue({ startDate: moment(this.applyLeaveDetails.get('startDate').value).format('YYYY-MM-DD') })
    }
    else {
      this.applyLeaveDetails.patchValue({ startDate: "" })
    }
    if (moment(this.applyLeaveDetails.get('endDate').value).format('YYYY-MM-DD') != "Invalid date") {
      this.applyLeaveDetails.patchValue({ endDate: moment(this.applyLeaveDetails.get('endDate').value).format('YYYY-MM-DD') })
    }
    else {
      this.applyLeaveDetails.patchValue({ endDate: "" })
    }

    if (this.applyLeaveDetails.get('includeWeekend').value != undefined) {
      this.applyLeaveDetails.patchValue({ includeWeekend: this.applyLeaveDetails.get('includeWeekend').value })
    }
    else {
      this.applyLeaveDetails.patchValue({ includeWeekend: "" })
    }

    this.applyLeaveDetails.patchValue({ "leaveRequestType": this.applyLeaveDetails.get('leaveRequestType').value })
    this.applyLeaveDetails.patchValue({ "category": this.applyLeaveDetails.get('category').value })

    this.searchapi.postApi(this.applyLeaveUrl, this.applyLeaveDetails.value).subscribe(res => {
      this.applyLeaveDetails.patchValue({
        "leaveRequestType": "Full Day",
        "startDate": null,
        "endDate": null,
        "startTime": "",
        "endTime": "",
        "category": "",
        "includeWeekend": "No",
        "reason": "",
        "otherCategory": "",
      });

      this.notificationAlert('Apply Leave')
    }, err => {
      //console.log(err, 'error')
    })
  }


  //To update the Leave information
  update() {
    
    console.log()
    let leaveData = {
      "id": this.duplicateShowLeaveEditData.value.Id,
      "leaveRequestType": this.duplicateShowLeaveEditData.value.LeaveType,
      "startDate": moment(this.duplicateShowLeaveEditData.value.StartDate).format('YYYY-MM-DD'),
      "endDate": moment(this.duplicateShowLeaveEditData.value.EndDate).format('YYYY-MM-DD'),
      "startTime": this.duplicateShowLeaveEditData.value.startTime,
      "endTime": this.duplicateShowLeaveEditData.value.endTime,
      "category": this.duplicateShowLeaveEditData.value.category,
      "includeWeekend": this.duplicateShowLeaveEditData.value.includeWeekend,
      "reason": this.duplicateShowLeaveEditData.value.Reason,
      "otherCategory": this.duplicateShowLeaveEditData.value.otherCategory,
      "reportsTo": JSON.parse(localStorage.getItem('userDetails')).ReportsTo,
      "departmentId": JSON.parse(localStorage.getItem('userDetails')).DepartmentId

    }

    this.searchapi.postApi(this.updateUrl, leaveData)
      .subscribe(res => {
        if (res) {

          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].StartDate = this.duplicateShowLeaveEditData.value.StartDate;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].EndDate = this.duplicateShowLeaveEditData.value.EndDate;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].IncludeWeekend = this.duplicateShowLeaveEditData.value.includeWeekend;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].LeaveCategory = this.duplicateShowLeaveEditData.value.category;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].leaveType =
            this.duplicateShowLeaveEditData.value.LeaveRequestType;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].OtherCategory =
            this.duplicateShowLeaveEditData.value.otherCategory;
          this.LeavesHistory[parseInt(this.duplicateShowLeaveEditData.value.ElementNumber)].Reason =
            this.duplicateShowLeaveEditData.value.Reason;

          this.notificationAlert('Update Leave')

        }
      }, err => {
        //console.log(err, 'error')
      })
  }

  cancelLeave() {
    //API to cancel the leave
    
    this.searchapi.postApi(this.cancelLeaveUrl, {
      "id": this.duplicateShowLeaveEditData.value.Id.toString(),
      "startDate": this.duplicateShowLeaveEditData.value.StartDate,
      "status": "Cancelled"
    })
      .subscribe((data) => {
        console.log(data, 'data after cancelling Leave')
      })
  }

  notificationAlert(data) {
    setTimeout(() => {
    
      if (data === 'Update Leave') {
        this.notificationModel.message = 'Leave Information updated succesfully!!';
      }
      else if (data === 'Apply Leave') {
        this.notificationModel.message = 'Leave applied succesfully!!';
      }
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });
  }

}