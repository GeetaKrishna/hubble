import { Component, OnInit } from '@angular/core';
import { HttpRequest, HttpClient } from '@angular/common/http';
import { Jsonp } from '@angular/http';
@Component({
  selector: 'app-heir-two',
  templateUrl: './heir-two.component.html',
  styleUrls: ['./heir-two.component.scss']
})
export class HeirTwoComponent implements OnInit {
  showSecondLevelChild: boolean;
  showFirstLevelChild: boolean = true;
  data: any;

  myData = [
    {
      name: "Chanakya Lokam",
      role: "Director",
      imgUrl: "assets/img/profiles/9x.jpg",
      showChildrens: true,
      children: [{
        name: "Siva Kusampudi",
        role: "Manager",
        imgUrl: "assets/img/profiles/6x.jpg",
        showChildrens: true,
        children: [
          {
            name: "Deepika Rangoori",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: [
              {
                name: "Sravani Ganti",
                role: "Webmaster",
                imgUrl: "assets/img/profiles/3x.jpg",
                showChildrens: false,
              }
            ]
          },
          {
            name: "Srinivas Patnala",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: [
              {
                name: "Janaki Ram",
                role: "Webmaster",
                imgUrl: "assets/img/profiles/3x.jpg",
                showChildrens: false,
              }
            ]
          },
          {
            name: "Pooja Dangeti",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: [
              {
                name: "Manoj Kumar",
                role: "Webmaster",
                imgUrl: "assets/img/profiles/3x.jpg",
                showChildrens: false,
              },
              {
                name: "Chandra Mutha",
                role: "Webmaster",
                imgUrl: "assets/img/profiles/3x.jpg",
                showChildrens: false,
              }
            ]
          },
          {
            name: "Praveen kommula",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: [

            ]
          }

        ]
      },
      {
        name: "Diana Ponduri",
        role: "Manager",
        imgUrl: "assets/img/profiles/6x.jpg",
        showChildrens: false,
        children: [
          {
            name: "Gowtham Sala",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: []
          },
          {
            name: "Prabhakar",
            role: "Webmaster",
            imgUrl: "assets/img/profiles/3x.jpg",
            showChildrens: false,
            children: []
          }
        ]
      }
      ]
    }
  ]
  constructor(public http: HttpClient) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/team-structure.json`);

    req.onload = () => {
      console.log(JSON.parse(req.response));
      this.data = JSON.parse(req.response);
      console.log(this.data[0].children[0]);
    };
    req.send();
  }


  showChild1(parent) {
    this.showFirstLevelChild = !this.showFirstLevelChild
    console.log(parent);
    console.log(this.myData.indexOf(parent));
    this.myData[this.myData.indexOf(parent)].showChildrens = !this.myData[this.myData.indexOf(parent)].showChildrens;

  }
  showChild2(a, parent) {
    for (let i = 0; i < this.myData[this.myData.indexOf(parent)].children.length; i++) {
      if (this.myData[this.myData.indexOf(parent)].children[i].showChildrens == true && this.myData[this.myData.indexOf(parent)].children[this.myData[this.myData.indexOf(parent)].children.indexOf(a)].showChildrens == false) {
        this.myData[this.myData.indexOf(parent)].children[i].showChildrens = false;
        console.log('inside if' + this.myData[this.myData.indexOf(parent)].children[i].showChildrens);

      }
      // console.log('inside for after if'+this.myData[this.myData.indexOf(parent)].children[i].showChildrens);

    }
    // console.log(this.myData.indexOf(parent));
    this.myData[this.myData.indexOf(parent)].children.indexOf(a);
    // console.log(this.myData[this.myData.indexOf(parent)].children.indexOf(a));

    this.myData[this.myData.indexOf(parent)].children[this.myData[this.myData.indexOf(parent)].children.indexOf(a)].showChildrens = !this.myData[this.myData.indexOf(parent)].children[this.myData[this.myData.indexOf(parent)].children.indexOf(a)].showChildrens;
    console.log('after if and for' + this.myData[this.myData.indexOf(parent)].children[this.myData[this.myData.indexOf(parent)].children.indexOf(a)].showChildrens);



    //  this.showSecondLevelChild = !this.showSecondLevelChild
  }

  showChild3(b, a, parent) {
    let levelOne = this.myData.indexOf(parent);
    let levelTwo = this.myData[this.myData.indexOf(parent)].children.indexOf(a);
    let levelThree = this.myData[this.myData.indexOf(parent)].children[this.myData[this.myData.indexOf(parent)].children.indexOf(a)].children.indexOf(b);



    let levelfourlength = this.myData[levelOne].children[levelTwo].children[levelThree].children.length;
    console.log(levelfourlength);

    console.log(levelThree);
    //true show childred


    for (let i = 0; i < this.myData[levelOne].children[levelTwo].children.length; i++) {
      if (this.myData[levelOne].children[levelTwo].children[i].showChildrens == true && this.myData[levelOne].children[levelTwo].children[levelThree].showChildrens == false) {
        this.myData[levelOne].children[levelTwo].children[i].showChildrens = false;
      }
    }


    this.myData[levelOne].children[levelTwo].children[levelThree].showChildrens = !this.myData[levelOne].children[levelTwo].children[levelThree].showChildrens;



  }






  ngOnInit() {
  }

}
