import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeirTwoComponent } from './heir-two.component';

describe('HeirTwoComponent', () => {
  let component: HeirTwoComponent;
  let fixture: ComponentFixture<HeirTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeirTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeirTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
