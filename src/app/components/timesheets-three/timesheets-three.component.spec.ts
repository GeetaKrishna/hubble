import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesheetsThreeComponent } from './timesheets-three.component';

describe('TimesheetsThreeComponent', () => {
  let component: TimesheetsThreeComponent;
  let fixture: ComponentFixture<TimesheetsThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesheetsThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesheetsThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
