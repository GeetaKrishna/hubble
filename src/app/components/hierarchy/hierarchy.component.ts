import { Component, OnInit, Injectable, ElementRef, Renderer2 } from '@angular/core';
import { Http } from '@angular/http';

import {TreeModule} from 'primeng/tree';
import {TreeNode} from 'primeng/api';

import { RestApiService } from '../../services/rest-api.service';

// API to get heirarchy details
@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})
@Injectable()
export class HierarchyComponent implements OnInit {

  files: TreeNode[];

  constructor(private restService: RestApiService,private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {
      this.restService.getFiles().then((files) =>{
        console.log(files, 'fles');
        //set expanded to true to expand by default
        this.files = files;
      }) 
  }
// 
  s(a){
    alert(a);
  }

}