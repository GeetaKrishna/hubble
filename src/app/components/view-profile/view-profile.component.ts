import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { ActivatedRoute, Router, NavigationEnd, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { RestApiService } from '../../services/rest-api.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss'],

})
export class ViewProfileComponent implements OnInit {
  selectedTitles: any = 'Employment';
  slideUp: any = {
    type: "md"
  }
  @ViewChild('mdSlideUp') public mdSlideUp: ModalDirective;
  searchUrl = environment.getEmployee;
  userDetails: any = {};
  currentUserId: string;
  constructor(private route: ActivatedRoute, private searchapi: RestApiService, private navCtrl: NgxNavigationWithDataComponent, private router: Router) {
  }
  showSlideUp(data) {
    this.selectedTitles = data;
    switch (this.slideUp.type) {
      case "md": {
        this.mdSlideUp.show();

        break;
      }
    }
  }
  ngOnInit() {
    console.log('navCtrl', this.navCtrl.data['id'])
    this.currentUserId = JSON.parse(localStorage.getItem('userDetails')).EmpId;

    // navCtrl mightnot work in Angular 8, so use state based and remove the ngx-navigation-with-data
    if (this.navCtrl.data['id']) {
      this.searchapi.postApi(this.searchUrl, {"empId": this.navCtrl.data['id']})
        .subscribe((list) => {
          console.log(list)
          this.userDetails = list;

          this.userDetails.SkillSet = this.userDetails.SkillSet.split(/[,]+/);
          if ((this.userDetails.SkillSet.length == 1) && (this.userDetails.SkillSet[0] == "" || " ")) {
            this.userDetails.SkillSet.pop();
          }
          this.userDetails.Email1 = "mailto:" + this.userDetails.Email1;

        })
    }
    else{
      this.searchapi.postApi(this.searchUrl, {'empId': JSON.parse(localStorage.getItem('userDetails')).EmpId})
        .subscribe((list) => {
          console.log(list)
          this.userDetails = list;

          this.userDetails.SkillSet = this.userDetails.SkillSet.split(/[,]+/);
          if ((this.userDetails.SkillSet.length == 1) && (this.userDetails.SkillSet[0] == "" || " ")) {
            this.userDetails.SkillSet.pop();
          }
          this.userDetails.Email1 = "mailto:" + this.userDetails.Email1;
        })
    }
  }
  ngOnDestroy(){
    //this.navCtrl.data['id'] = undefined;
  }
}
