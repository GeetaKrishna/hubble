import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  selectedOption;
  gender = [
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },
  ];

  maritialStatus = [
    { value: 'single', label: 'Single' },
    { value: 'married', label: 'Married' },
    { value: 'divorced', label: 'Divorced' },
  ];

  country = [
    { value: 'india', label: 'India' },
    { value: 'australia', label: 'Australia' },
    { value: 'usa', label: 'USA' },
  ];
  constructor() { }

  ngOnInit() {
  }

}
