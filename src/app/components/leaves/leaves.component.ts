import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrls: ['./leaves.component.scss']
})
export class LeavesComponent implements OnInit {
  me :boolean = true;
  resourceRole: boolean = false;
  constructor() { }

  ngOnInit() {
    this.resourceRole = JSON.parse(localStorage.getItem('userDetails')).IsManager || JSON.parse(localStorage.getItem('userDetails')).IsTeamLead;
  }

}
