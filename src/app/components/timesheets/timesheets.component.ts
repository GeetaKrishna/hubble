import { Component, OnInit,ViewChild } from '@angular/core';

import { Title } from '@angular/platform-browser';

import { ModalDirective } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-timesheets',
  templateUrl: './timesheets.component.html',
  styleUrls: ['./timesheets.component.scss']
})
export class TimesheetsComponent implements OnInit {
  flipDiv:boolean;
  name:string;
  date:string;
 full:boolean;
 daysCounter = 0;
  showUnmark = false;
  showSubmit = false;

  days = [
    {
      name:'SUNDAY',
      date:'17th February, 2018',
      flip: false, status:'',
      counter: 0,
        
    },
    {
      name:'MONDAY',
      date:'18th February, 2018',
      flip: false, status:'',
      counter: 0
    },
    {
      name:'TUESDAY',
      date:'19th February, 2018',
      flip: false, status:'',
      counter: 0
    },
    {
      name:'WEDNESDAY',
      date:'20th February, 2018',
      flip: false, status:'',
      counter: 0
    },
    {
      name:'THURSDAY',
      date:'21st February, 2018',
      flip: false, status:'',
      counter: 0
    },
    {
      name:'FRIDAY',
      date:'22nd February, 2018',
      flip: false, status:'',
      counter: 0
    },
    {
      name:'SATURDAY',
      date:'23rd February, 2018',
      flip: false, status:'',
      counter: 0
    }
  ];



  stickUp:any = {
    type:"lg"
  };
 
 
 
title:string= 'Timesheets | Hubble';

@ViewChild('mdStickUp') mdStickUp: ModalDirective;

constructor(private titleService: Title ) {
   this.titleService.setTitle(this.title);    
};

flipa(day){
  this.flipDiv = !this.flipDiv;
  console.log(day.name);
  console.log(this.days.indexOf(day));
  this.days.indexOf(day);
  this.days[this.days.indexOf(day)].flip = !this.days[this.days.indexOf(day)].flip;
  
}

showStickUp(){
  switch(this.stickUp.type) { 
    case "md": { 
      this.mdStickUp.show();
       break; 
    } 
    case "lg": { 
      this.mdStickUp.show();
       break; 
    } 
   
 } 
}



fullday(day){
  this.days.indexOf(day);
  this.days[this.days.indexOf(day)].status = 'Full Day';
  
  this.flipa(day);

  if (this.days[this.days.indexOf(day)].counter === 0){
    this.days[this.days.indexOf(day)].counter += 1;
   }

   console.log('fullday' +this.days[this.days.indexOf(day)].counter);

   this.calculations();
}
halfday(day){
  this.days.indexOf(day);
  this.days[this.days.indexOf(day)].status = 'Half Day';
  
  this.flipa(day);

  if (this.days[this.days.indexOf(day)].counter === 0){
    this.days[this.days.indexOf(day)].counter += 1;
   }

   console.log('halfday' +this.days[this.days.indexOf(day)].counter);

   this.calculations();
}
leave(day){
  this.days.indexOf(day);
  this.days[this.days.indexOf(day)].status = 'Leave';
  this.flipa(day);
  
  if (this.days[this.days.indexOf(day)].counter === 0){
    this.days[this.days.indexOf(day)].counter += 1;
   }


   console.log('leave' +this.days[this.days.indexOf(day)].counter);
   this.calculations();
   
}
holiday(day){
  this.days.indexOf(day);
  this.days[this.days.indexOf(day)].status = 'Holiday';
  this.flipa(day);
  if (this.days[this.days.indexOf(day)].counter === 0){
    this.days[this.days.indexOf(day)].counter += 1;
   }

   console.log('holiday' +this.days[this.days.indexOf(day)].counter);
   this.calculations();
}

markRegular(){

  for(let i =0; i < this.days.length; i++){
    this.days[i].status = 'Full Day';
  }
  this.days[0].status = '';
  this.days[6].status = '';

  this.calculations();
  // this.daysCounter = 5;
  this.showUnmark = true;
  this.showSubmit = true;
  



}

unMark(){
  for(let i=0; i < this.days.length; i++){
    this.days[i].status = '';
    this.days[i].counter = 0;
  }
  this.calculations();
  this.showUnmark = false;
  this.showSubmit = false;
 
}

calculations(){
  this.daysCounter = this.days[0].counter + this.days[1].counter + this.days[2].counter + this.days[3].counter + this.days[4].counter + this.days[5].counter + this.days[6].counter;
  console.log(this.daysCounter);
  if(this.daysCounter > 0){
    this.showUnmark = true;
  }
  if(this.daysCounter >= 5){
    this.showSubmit = true;
  }
  
}


submit(){
  this.daysCounter = 0;
  for(let i= 0; i< this.days.length; i++){
    this.days[i].counter = 0;
  }

  this.unMark();
}


  ngOnInit() {
  }


  
}

