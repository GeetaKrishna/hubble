import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeSearch3Component } from './employee-search3.component';

describe('EmployeeSearch3Component', () => {
  let component: EmployeeSearch3Component;
  let fixture: ComponentFixture<EmployeeSearch3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeSearch3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeSearch3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
