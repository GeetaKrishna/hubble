import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-search3',
  templateUrl: './employee-search3.component.html',
  styleUrls: ['./employee-search3.component.scss']
})
export class EmployeeSearch3Component implements OnInit {
  selectedOption;
  department = [
  { value: 'marketing', label: 'Marketing' },
  { value: 'development', label: 'Development' },
  { value: 'sales', label: 'Sales' },
  { value: 'recruitment', label: 'Recruitment' },
  ];

  location = [
    { value: 'miracle city', label: 'Miracle City' },
    { value: 'miracle heights', label: 'Miracle Heights' },
    { value: 'headquarters', label: 'Headquarters' },
    { value: 'miracle valley', label: 'Miracle Valley' },
    ];

    designation =  [
      { value: 'lead', label: 'Lead' },
      { value: 'manager', label: 'Manager' },
      { value: 'director', label: 'Director' },
      { value: 'vp', label: 'Pre-sales' },
      ];

      contactList = [
        {
          imgUrl:'assets/img/profiles/4x.jpg',
          designation:'Web Master',
          email:'rgottimukkala1',
          name:'Rajeev Gottimukkala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle Heights',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/7x.jpg',
          designation:'Web Master',
          email:'crangoori',
          name:'Chaitanya Rangoori',
          voip:'(272-412)-27028 ',
          mobile:'9502195091',
          location:'Miracle Heights',
          since:'Since 21st May, 2014',

        },
        {
          imgUrl:'assets/img/profiles/bd2x.jpg',
          designation:'Web Master',
          email:'slekala',
          name:'Shoba Lekala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Novi, Michigan',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/6x.jpg',
          designation:'Web Master',
          email:'vkusampudi',
          name:'Siva Kusampudi',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle City',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/4x.jpg',
          designation:'Web Master',
          email:'rgottimukkala1',
          name:'Rajeev Gottimukkala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle Heights',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/7x.jpg',
          designation:'Web Master',
          email:'crangoori',
          name:'Chaitanya Rangoori',
          voip:'(272-412)-27028 ',
          mobile:'9502195091',
          location:'Miracle Heights',
          since:'Since 21st May, 2014',

        },
        {
          imgUrl:'assets/img/profiles/bd2x.jpg',
          designation:'Web Master',
          email:'slekala',
          name:'Shoba Lekala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Novi, Michigan',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/6x.jpg',
          designation:'Web Master',
          email:'vkusampudi',
          name:'Siva Kusampudi',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle City',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/4x.jpg',
          designation:'Web Master',
          email:'rgottimukkala1',
          name:'Rajeev Gottimukkala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle Heights',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/7x.jpg',
          designation:'Web Master',
          email:'crangoori',
          name:'Chaitanya Rangoori',
          voip:'(272-412)-27028 ',
          mobile:'9502195091',
          location:'Miracle Heights',
          since:'Since 21st May, 2014',

        },
        {
          imgUrl:'assets/img/profiles/bd2x.jpg',
          designation:'Web Master',
          email:'slekala',
          name:'Shoba Lekala',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Novi, Michigan',
          since:'Since 21st July, 2015',

        },
        {
          imgUrl:'assets/img/profiles/6x.jpg',
          designation:'Web Master',
          email:'vkusampudi',
          name:'Siva Kusampudi',
          voip:'(272-412)-27028 ',
          mobile:'7396994195',
          location:'Miracle City',
          since:'Since 21st July, 2015',

        }
      ]
  constructor() { }

  ngOnInit() {
  }

}
