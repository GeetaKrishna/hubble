import { Component, OnInit, ViewChild, ViewEncapsulation, NgModule } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { RestApiService } from '../../services/rest-api.service';
import { environment } from '../../../environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'app-leave-approvals',
  templateUrl: './leave-approvals.component.html',
  styleUrls: ['./leave-approvals.component.scss']
})

export class LeaveApprovalsComponent implements OnInit {
  @ViewChild('mdSlideUpApproveLeave') public mdSlideUpApproveLeave: ModalDirective;
  @ViewChild('mdSlideUpRejectLeave') public mdSlideUpRejectLeave: ModalDirective;
  @ViewChild('mdSlideUpLeaveHistory') public mdSlideUpLeaveHistory: ModalDirective;

  subOrdinateSearchUrl = environment.searchLeavesOfSubOrdinates;
  getTeamMembersUrl = environment.getTeamMembers;
  availableLeavesUrl = environment.availableLeaves;
  individualLeavesUrl = environment.individualLeaves;
  leavesUrl = environment.searchLeavesOfEmployee;

  p = 1;
  opacity;
  //Date Range Setup
  _startDate = null;
  _endDate = null;
  newArray = (len) => {
    const result = [];
    for (let i = 0; i < len; i++) {
      result.push(i);
    }
    return result;
  };
  _startValueChange = () => {
    if (this._startDate > this._endDate) {
      this._endDate = null;
    }
  };
  _endValueChange = () => {
    if (this._startDate > this._endDate) {
      this._startDate = null;
    }
  };
  _disabledStartDate = (startValue) => {
    if (!startValue || !this._endDate) {
      return false;
    }
    return startValue.getTime() >= this._endDate.getTime();
  };
  _disabledEndDate = (endValue) => {
    if (!endValue || !this._startDate) {
      return false;
    }
    return endValue.getTime() <= this._startDate.getTime();
  };

  panels = [
    {
      active: true,
      name: 'This is panel header 1',
      disabled: false,
      childPanel: [
        {
          active: false,
          name: 'This is panel header 1-1'
        }
      ]
    },
    {
      active: false,
      disabled: true,
      name: 'This is panel header 2'
    },
    {
      active: true,
      disabled: false,
      name: 'This is panel header 3'
    }
  ];

  tabs = [
    {
      name: 'Tab 1',
      content: 'Content of Tab Pane 1'
    },
    {
      name: 'Tab 2',
      content: 'Content of Tab Pane 2'
    },
    {
      name: 'Tab 3',
      content: 'Content of Tab Pane 3'
    }
  ];
  projects = [
    'Sunsweet', 'Hubble'
  ]
  showOtherTitle: boolean = false;
  selectedLeaveCategory;

  TeamLeavesHistory = [

    // {
    //   appliedBy: 'Srinivas Patnala',
    //   startDate: '02/05/2017',
    //   endDate: '03/05/2017',
    //   days: 2,
    //   type: 'Vacation',
    //   submittedDate: '08/05/2017',
    //   status: 'Cancelled',
    //   approvedDate: '12/05/2017',
    //   reportsTo: 'vkusampudi'
    // }
  ]

  LeaveApprovalHistory = [
    {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }, {

      startDate: '02/05/2017',
      endDate: '03/05/2017',
      days: 2,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    },
    {

      startDate: '12/05/2017',
      endDate: '12/07/2017',
      days: 2,
      type: 'Comp Off',
      category: 'Others',
      status: 'Cancelled'
    },
    {

      startDate: '12/07/2017',
      endDate: '12/08/2017',
      days: 1,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    },
    {

      startDate: '12/07/2017',
      endDate: '12/08/2017',
      days: 1,
      type: 'Vacation',
      category: 'Sick',
      status: 'Approved'
    }

  ]

  teamMembers = [
    {
      value: 'All', label: 'All'
    }
  ]

  leaveStatus = [
    {
      value: 'All', label: 'All'
    },
    {
      value: 'Applied', label: 'Applied'
    },
    {
      value: 'Approved', label: 'Approved'
    },
    {
      value: 'Cancelled', label: 'Cancelled'
    }

  ]

  LeaveCategory = [
    {
      value: '', label: 'Sick'
    },
    {
      value: '', label: 'Family Care'
    },
    {
      value: '', label: 'Govt Works'
    },
    {
      value: '', label: 'Others'
    },
  ]
// 
  includeWeekends = [
    { value: '', label: 'Yes' },
    { value: '', label: 'No' },
  ]
  showApprovalData: any =
    {
      appliedBy: '',
      startDate: new Date(),
      endDate: new Date(),
      days: 1,
      type: '',
      submittedDate: new Date(),
      status: '',
      approvedDate: new Date(),
      reportsTo: '',
      othersOnLeave: []
    };
  p1: number = 1;
  selectedEmployee: any;
  availableLeaves: any;
  test: any;
  TeamLeavesHistoryOriginal: any;
  selectedLeaveStatus: any = "";
  tags: any = [];
  leaveRecords: any;

  constructor(private api: RestApiService) { }

  showReject() {

    switch ('md') {
      case "md": {
        this.mdSlideUpRejectLeave.show();
        break;
      }
    }
  }

  showLeaveHistory(empId, Id) {
    console.log(empId, 'EmployeeId', Id)
    switch ('md') {
      case "md": {
        this.api.postApi(this.leavesUrl, {
          "empId": empId.toString(),
          "startDate": '',
          "endDate": '',
          "status": ''
        })
        .subscribe((data)=>{
          console.log(data);
          this.leaveRecords = data;
        })
        this.api.postApi(this.availableLeavesUrl, { "empNo": empId })
          .subscribe((leaves) => {
            this.availableLeaves = leaves.AvialbleLeaves;
            this.mdSlideUpLeaveHistory.show();
          })
        break;
      }
    }
  }

  showApproval(data) {
    switch ('md') {
      case "md": {
        console.log(data)
        this.showApprovalData = data;
        this.api.postApi(this.individualLeavesUrl, {
          "empId": data.EmpId,
          "id": data.Id
        })
          .subscribe((individualLeave) => {
            this.tags = [];
              for(let i=0; i<individualLeave.OtherOnLeave.length; i++){
                if(individualLeave.OtherOnLeave[i] != ""){
                  this.tags.push(individualLeave.OtherOnLeave[i]);                  
                }
              }
            // this.tags = individualLeave.OtherOnLeave
            console.log(this.showApprovalData)
            this.mdSlideUpApproveLeave.show();
          })

        break;
      }
    }
  }

  changeLeaveCategory() {
    console.log(this.selectedLeaveCategory);
    if (this.selectedLeaveCategory.label == 'Others') {
      this.showOtherTitle = true;
    }
    else
      this.showOtherTitle = false;

  }
  getStatus(data) {

    switch (data) {
      case 'Applied':
        return '#00aae7';
      case 'Cancelled':
        return '#f35958';
      case 'Approved':
        return '1dbb99';
    }

  }
  finalNumber() {
    return (((((this.p > 1 ? this.p : 1) - 1) * 4 + 4) > this.LeaveApprovalHistory.length) ? (this.LeaveApprovalHistory.length) : ((((this.p > 1 ? this.p : 1) - 1) * 4) + 4));
  }
  finalNumber1() {
    return (((((this.p1 > 1 ? this.p1 : 1) - 1) * 10 + 10) > this.TeamLeavesHistory.length) ? (this.TeamLeavesHistory.length) : ((((this.p1 > 1 ? this.p1 : 1) - 1) * 10) + 10));
  }

  ngOnInit() {

    this.api.postApi(this.getTeamMembersUrl, { "loginId": JSON.parse(localStorage.getItem('userDetails')).LoginId })
      .subscribe((teamMembers) => {
        console.log('teamMembers', teamMembers);
        Object.keys(teamMembers).map((teamMember) => {
console.log(teamMember, teamMembers[teamMember])
          //store for future reference and to show in Employee List for Search
          this.teamMembers.push({ value: teamMember, label: teamMembers[teamMember] })

        })
      })

    this.search('allLeaves')
  }

subOrdinateAPICall(obj, typeOfLeaves){
console.log(obj, typeOfLeaves)
  this.api.postApi(this.subOrdinateSearchUrl, obj)
  .subscribe((result) => {
    if(typeOfLeaves === 'allLeaves'){
      this.TeamLeavesHistoryOriginal = result;
    }
    this.TeamLeavesHistory = result;
  })

}

  search(typeOfLeaves) {

      console.log(this.selectedEmployee, this.selectedLeaveStatus, this._endDate, this._startDate)

      let endDate = moment(this._endDate).format("YYYY-MM-DD");
      let startDate = moment(this._startDate).format("YYYY-MM-DD");

      if ((this.selectedEmployee === "All") || (this.selectedEmployee === null) || (this.selectedEmployee === undefined)) {
        this.selectedEmployee = "";
      }

      if ((this.selectedLeaveStatus === null) || (this.selectedLeaveStatus === undefined)) {
        this.selectedLeaveStatus = "";
      }

      if (endDate === "Invalid date") {
        endDate = "";
      }

      if (startDate === "Invalid date") {
        startDate = "";
      }

      let obj = {
        "loginId": JSON.parse(localStorage.getItem('userDetails')).LoginId,
        "startDate": startDate,
        "endDate": endDate,
        "status": this.selectedLeaveStatus,
        "teamMemberLoginId": this.selectedEmployee
      };
 
      //Get Leaves list of Employee
      this.subOrdinateAPICall(obj, typeOfLeaves);
 
    }

}