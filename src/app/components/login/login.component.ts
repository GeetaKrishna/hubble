import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../@pages/components/message/message.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { RestApiService } from '../../services/rest-api.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  notificationModel: any = {
    heading: "Simple Alert",
    desc: "Awesome Loading Circle Animation",
    position: "bottom-right",
    type: "simple"
  }
  selectedColor = 'miracle';
  loginURL: any = environment.loginURL;
  notification: any;
  userName: string;
  password: string;

  message: any;

  createBasicNotification() {
    // setTimeout(() => {
    // this.notificationModel.position = this.nofitcationStrings[0]["position"];
    // this.notificationModel.type = this.nofitcationStrings[0]["type"];
    this.notificationModel.message = 'Details sent. Please check your email';
    this.notificationModel.color = this.selectedColor;
    //Create Notification

    this._notification.create(
      this.notificationModel.color,
      this.notificationModel.message,
      {
        Position: this.notificationModel.position,
        Style: this.notificationModel.type,
        Duration: 50000
      });
    // }, 1500);
  }



  constructor(private _notification: MessageService, private loginapi: RestApiService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    let data = { "loginId": this.userName, "password": this.password }
    this.loginapi.postApi(this.loginURL, data).subscribe(access => {
      console.log("res", access)
      if (access.hasOwnProperty('LoginId')) {
          
//this.loginapi.updateUserDetails(access)
localStorage.setItem('userDetails', JSON.stringify(access));
// sessionStorage.setItem('userDetails', JSON.stringify(access));
this.router.navigateByUrl('/dashboard');
this.loginapi.setLoggedIn(true)
      }else if(access === {}){
        this.router.navigateByUrl('/');
      } else {
        console.log("success")
        this.router.navigateByUrl('/');
      }
    })
  }

}
