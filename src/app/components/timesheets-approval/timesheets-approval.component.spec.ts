import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesheetsApprovalComponent } from './timesheets-approval.component';

describe('TimesheetsApprovalComponent', () => {
  let component: TimesheetsApprovalComponent;
  let fixture: ComponentFixture<TimesheetsApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesheetsApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesheetsApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
