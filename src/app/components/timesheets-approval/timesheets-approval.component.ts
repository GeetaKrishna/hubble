import { Component, OnInit ,ViewChild} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.directive';
import { MessageService } from '../../@pages/components/message/message.service';
@Component({
  selector: 'app-timesheets-approval',
  templateUrl: './timesheets-approval.component.html',
  styleUrls: ['./timesheets-approval.component.scss']
})
export class TimesheetsApprovalComponent implements OnInit {
  @ViewChild('mdSlideUpApproveLeave') public mdSlideUpApproveLeave: ModalDirective;
  @ViewChild('mdSlideUpApproveLeave1') public mdSlideUpApproveLeave1: ModalDirective;
  @ViewChild('mdSlideUpRejectLeave') public mdSlideUpRejectLeave: ModalDirective;
  @ViewChild('mdSlideUpSuccessApprove') public mdSlideUpSuccessApprove : ModalDirective;
  @ViewChild('mdSlideUpRejectApprove') public mdSlideUpRejectApprove : ModalDirective;
  isShow:boolean = true;
  selectedColor = 'miracle';
  startDate='Feb 06 2019'
  endDate='Feb 12 2019'
  subDate='Aug 05 2019'
  Timesheetsapproval= [

    {
      name: 'Rajeev Gottimukkala', 
      startDate: '02/06/17',
      endDate: '02/12/17',     
         
      billable:40,   
      submittedDate: '08/05/17',
      status: 'Submitted',           
    }          
  ]
  Timesheetsapproval1= [

    {
      name: 'Rajeev Gottimukkala', 
      startDate: '02/06/17',
      endDate: '02/12/17',              
      billable:40,   
      submittedDate: '08/05/17',
      status: 'Rejected',           
    },
  ]
  teamMembers = [
    {
      value: 'All', label: 'All'
    },
    {
      value: 'Rajeev', label: 'Rajeev Gottimukkala'
    },
    {
      value: 'Deepika', label: 'Deepika Rangoori'
    },
    {
      value: 'shoba', label: 'Shoba Lekala'
    },
    {
      value: 'sravani', label: 'Sravani Ganti'
    },
  ]

  year = [
    {
      value: '2017', label: '2017'
    },
    {
      value: '2018', label: '2018'
    },
    {
      value: '2019', label: '2019'
    },
    
  ]

  month = [
    {
      value: 'Jan', label: 'Jan'
    },
    {
      value: 'Feb', label: 'Feb'
    },
    {
      value: 'Mar', label: 'Mar'
    },
    {
      value: 'Apr', label: 'Apr'
    },
  
  {
    value: 'May', label: 'May'
  },
  {
    value: 'Jun', label: 'Jun'
  },
  {
    value: 'Jul', label: 'Jul'
  },
  {
    value: 'Aug', label: 'Aug'
  },
  {
    value: 'Sep', label: 'Sep'
  },
  {
    value: 'Oct', label: 'Oct'
  },
  {
    value: 'Nov', label: 'Nov'
  },
  {
    value: 'Dec', label: 'Dec'
  },
]
  Status = [
    {
      value: 'all', label: 'All'
    },
    {
      value: 'submitted', label: 'Submitted'
    },
    {
      value: 'approved', label: 'Approved'
    },
    {
      value: 'rejected', label: 'Rejected'
    }

  ]
  notificationModel: any =    {
    heading:"Simple Alert",
    desc:"Awesome Loading Circle Animation",
    position:"bottom-right",
    type:"simple"
  }
  mdSlideUpApproveTimesheet: any;
  // mdSlideUpApproveLeave1: any;
  constructor(private _notification: MessageService) {


  }
  getStatus(data) {

    switch (data) {
      case 'Submitted':
        return '#00aae7';
      case 'Rejected':
        return '#f35958';
      case 'Approved':
        return '1dbb99';
    }

  } 

  showApprovalconfirmataion() {
    switch ('md') {
      case "md": {
        this.mdSlideUpApproveTimesheet.show();
        break;
      }
    }
  }

  showApproval() {
    switch ('md') {
      case "md": {
        this.mdSlideUpApproveLeave.show();
        break;
      }
    }
  }

  showApproval1() {
    switch ('md') {
      case "md": {
        this.mdSlideUpApproveLeave1.show();
        break;
      }
    }
  }
   showReject() {
    
    switch ('md') {
      case "md": {
        this.mdSlideUpRejectLeave.show();
        break;
      }
    }
  }

  showSuccessApprove() {
    switch('md') {
      case "md" : {
        this.mdSlideUpSuccessApprove.show();        
      }
    }
  }

  
  showRejectApprove() {
    switch('md') {
      case "md" : {
        this.mdSlideUpRejectApprove.show();        
      }
    }
  }

  createNotificationForApprove() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Timesheet Approved successfully';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
        this.mdSlideUpApproveLeave.hide();
    });

  }

  createNotificationForReject() {

    setTimeout(() => {
      // this.notificationModel.position = this.nofitcationStrings[0]["position"];
      // this.notificationModel.type = this.nofitcationStrings[0]["type"];
      this.notificationModel.message = 'Timesheet Rejected successfully';
      this.notificationModel.color = this.selectedColor;
      //Create Notification

      this._notification.create(
        this.notificationModel.color,
        this.notificationModel.message,
        {
          Position: this.notificationModel.position,
          Style: this.notificationModel.type,
          Duration: 3000
        });
    });

  }

  vacationtags = ["7'Feb"];
  comptimetags = ["8'Feb","9'Feb "];

  ngOnInit() {
   
  }



  showStatus(status:string)
  {
 
    if(status === 'Approved'){
      return true;
    }else{
      return false;
    }
}
// addApprovel(i){
//   console.log(i);
//   this.Timesheetsapproval[i].status = 'Approved'
// }
// addReject(i){
//   console.log(i);
//   this.Timesheetsapproval[i].status = 'Rejected'
// }
}
