import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { TreeNode } from 'primeng/api'

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  data: any = sessionStorage.getItem('userDetails') || {};

  private loggedInStatus = false;
  constructor(private http: HttpClient) {
    if(localStorage.getItem('userDetails')){
      this.loggedInStatus = true;
    }
   }

  setLoggedIn(value:boolean){
    this.loggedInStatus = value;
  }

  get isLoggedIn(){
    return this.loggedInStatus
  }

  //private userDetails = new BehaviorSubject('Basic Approval is required!');
  //currentuserDetails = this.userDetails.asObservable();
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'token': 'checking',
      'Access-Control-Allow-Origin':'*'
    })
  };

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getApi(endpoint): Observable<any> {
    return this.http.get(endpoint).pipe(
      map(this.extractData));
  }

  postApi(endpoint, data): Observable<any> {
    console.log(endpoint, data);
    return this.http.post<any>(endpoint, JSON.stringify(data), this.httpOptions).pipe(
      map((uresponse: Response) => {
        console.log("api res", uresponse)
        return uresponse;
      }), catchError(this.handleError<any>('Login'))
    );
  }

    getFiles() {
	  return this.http.get('assets/data/files.json')
				  .toPromise()
				  .then(res => <TreeNode[]> res.data);
	}
  
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
