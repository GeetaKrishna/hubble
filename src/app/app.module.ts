//Angular Core
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


//Routing
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

//Layouts
import { CondensedComponent, BlankComponent, RootLayout, CorporateLayout, SimplyWhiteLayout, ExecutiveLayout, CasualLayout } from './@pages/layouts';
//Layout Service - Required
import { pagesToggleService } from './@pages/services/toggler.service';

//Shared Layout Components
import { SidebarComponent } from './@pages/components/sidebar/sidebar.component';
import { QuickviewComponent } from './@pages/components/quickview/quickview.component';
import { QuickviewService } from './@pages/components/quickview/quickview.service';
import { SearchOverlayComponent } from './@pages/components/search-overlay/search-overlay.component';
import { HeaderComponent } from './@pages/components/header/header.component';
import { HorizontalMenuComponent } from './@pages/components/horizontal-menu/horizontal-menu.component';
import { SharedModule } from './@pages/components/shared.module';
import { pgListViewModule } from './@pages/components/list-view/list-view.module';
import { pgCardModule } from './@pages/components/card/card.module';
import { pgCardSocialModule } from './@pages/components/card-social/card-social.module';
import { pgSelectfx } from '../app/@pages/components/cs-select/select.module';
import { pgSelectModule } from '../app/@pages/components/select/select.module';
import { pgTagModule } from './@pages/components/tag/tag.module';

import { DatesS } from './DatesSuffix';

import {TreeModule} from 'primeng/tree';

//Basic Bootstrap Modules
import {
  BsDropdownModule,
  AccordionModule,
  AlertModule,
  ButtonsModule,
  CollapseModule,
  ProgressbarModule,
  TabsModule,
  TooltipModule,
  TypeaheadModule,
} from 'ngx-bootstrap';

//Pages Globaly required Components - Optional
import { pgTabsModule } from './@pages/components/tabs/tabs.module';
import { pgSwitchModule } from './@pages/components/switch/switch.module';
import { ProgressModule } from './@pages/components/progress/progress.module';
import { pgUploadModule } from './@pages/components/upload/upload.module';

//Thirdparty Components / Plugins - Optional
import { QuillModule } from 'ngx-quill';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';


//Sample Blank Pages - Optional
import { BlankCorporateComponent } from './@pages/layouts/blank-corporate/blank-corporate.component';
import { BlankSimplywhiteComponent } from './@pages/layouts/blank-simplywhite/blank-simplywhite.component';
import { BlankCasualComponent } from './@pages/layouts/blank-casual/blank-casual.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';

import { EmployeeSearchComponent } from './components/employee-search/employee-search.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LockComponent } from './components/lock/lock.component';
import { LeavesComponent } from './components/leaves/leaves.component';

import { ProfileComponent } from './components/profile/profile.component';
import { ModalModule } from 'ngx-bootstrap';
import { HierarchyComponent } from './components/hierarchy/hierarchy.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { EmployeeSearchOneComponent } from './components/employee-search-one/employee-search-one.component';
import { ViewProfileComponent } from './components/view-profile/view-profile.component';
import { ProfileNewComponent } from './components/profile-new/profile-new.component';
import { HeirTwoComponent } from './components/heir-two/heir-two.component';
import { pgDatePickerModule } from './@pages/components/datepicker/datepicker.module';
//import { HierarchyThreeComponent } from './components/hierarchy-three/hierarchy-three.component';
import { EmployeeSearch3Component } from './components/employee-search3/employee-search3.component';
import { TimesheetsComponent } from './components/timesheets/timesheets.component';
import { FlipModule } from 'ngx-flip';
import { MessageService } from './@pages/components/message/message.service';
import { MessageModule } from './@pages/components/message/message.module';
import { TimesheetsTwoComponent } from './components/timesheets-two/timesheets-two.component';
import { TimesheetsThreeComponent } from './components/timesheets-three/timesheets-three.component';
import { LeaveApprovalsComponent } from './components/leave-approvals/leave-approvals.component';

import { BmsComponent } from './components/bms/bms.component';
import { pgTimePickerModule } from './@pages/components/time-picker/timepicker.module';
import { FullCalendarModule } from '@fullcalendar/angular';
import { TimesheetsApprovalComponent } from './components/timesheets-approval/timesheets-approval.component';
import { StarPerformersComponent } from './components/star-performers/star-performers.component';
import { AuthGuard } from './guards/auth.guard';
import { BiometricComponent } from './components/biometric/biometric.component';
import { AppPasswordDirective } from './app-password.directive';
import { NgxNavigationWithDataComponent } from "ngx-navigation-with-data";

import { FileSelectDirective } from 'ng2-file-upload';
import { MyLeavesComponent } from './components/my-leaves/my-leaves.component';

import {NgxPaginationModule} from 'ngx-pagination';

//Module for infinite scroll that doesn't slow down the web page
import { VirtualScrollerModule } from 'ngx-virtual-scroller';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

//Hammer Config Overide
//https://github.com/angular/angular/issues/10541
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'pinch': { enable: false },
    'rotate': { enable: false }
  }
}

@NgModule({
  declarations: [
    AppComponent,
    CondensedComponent,
    CorporateLayout,
    SimplyWhiteLayout,
    ExecutiveLayout,
    CasualLayout,
    SidebarComponent, QuickviewComponent, SearchOverlayComponent, HeaderComponent, HorizontalMenuComponent,
    BlankComponent,
    RootLayout,
    BlankCorporateComponent,
    BlankSimplywhiteComponent,
    BlankCasualComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
	CalendarComponent,
    EmployeeSearchComponent,
    LockComponent,
    LeavesComponent,

    ProfileComponent,
    HierarchyComponent,
    EmployeeSearchOneComponent,
    ViewProfileComponent,
    ProfileNewComponent,
    HeirTwoComponent,
    //HierarchyThreeComponent,
    EmployeeSearch3Component,
    TimesheetsComponent,
    TimesheetsTwoComponent,
    TimesheetsThreeComponent,
    LeaveApprovalsComponent,
    BmsComponent,
    TimesheetsApprovalComponent,
    StarPerformersComponent,
    BiometricComponent,
    AppPasswordDirective,
	DatesS,
  FileSelectDirective,
  MyLeavesComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    ProgressModule,
    pgListViewModule,
    pgCardModule,
    pgUploadModule,
    pgDatePickerModule,
    pgTagModule,
    pgCardSocialModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgxPaginationModule,
    pgTabsModule,
    pgSelectfx,
    PerfectScrollbarModule,
    VirtualScrollerModule,
    pgSwitchModule,
    QuillModule,
    pgSelectModule,
    NgxDatatableModule,
    FlipModule,
    pgTimePickerModule,
    MessageModule,
	TreeModule,
	FullCalendarModule
  ],
  providers: [AuthGuard,QuickviewService, pagesToggleService, MessageService, {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: AppHammerConfig
    },     NgxNavigationWithDataComponent  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
