import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RestApiService } from '../services/rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
constructor(private auth:RestApiService, private router: Router){

}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.auth.isLoggedIn){
        //this.router.navigate(['/dashboard']);
        return this.auth.isLoggedIn;
      } else {
       // this.router.navigate(['/']);
       this.router.navigate(['/']);
        return this.auth.isLoggedIn;
      }
  }
}
