import { Pipe, PipeTransform } from '@angular/core';
// import { Injectable } from "@angular/core";
// import { NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";


//Code For dateSuffix starts here
@Pipe({ name: 'datesS' })
export class DatesS implements PipeTransform {
    transform(value: string): string {

    let suffix = 'th',
        day = value;

        if (day === '1' || day === '21' || day === '31') {
            suffix = 'st'
        } else if (day === '2' || day === '22') {
            suffix = 'nd';
        } else if (day === '3' || day === '23') {
           suffix = 'rd';
        }

        return suffix;

    }
}
//Code For dateSuffix ends here

//Code For Parsing the ngbDateFormat starts here.Using for all components
// function padNumber(value: number) {
//     if (isNumber(value)) {
//         return `0${value}`.slice(-2);
//     } else {
//         return "";
//     }
// }

// function isNumber(value: any): boolean {
//     return !isNaN(toInteger(value));
// }

// function toInteger(value: any): number {
//     return parseInt(`${value}`, 10);
// }


// @Injectable()
// export class NgbDateFRParserFormatt extends NgbDateParserFormatter {
//     parse(value: string): NgbDateStruct {
//         console.log(value)
//         if (value) {
//             const dateParts = value.trim().split('-');
//             console.log(dateParts[0])
//             if (dateParts.length === 1 && isNumber(dateParts[0])) {
//                 return { year: toInteger(dateParts[0]), month: null, day: null };
//             } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
//                 return { year: toInteger(dateParts[0]), month: toInteger(dateParts[1]), day: null };
//             } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
//                 console.log(dateParts[2])
//                 return { year: toInteger(dateParts[0]), month: toInteger(dateParts[1]), day: toInteger(dateParts[2]) };
//             }
//         }

//         return null;
//     }

//     format(date: NgbDateStruct): string {
//         console.log(date)
//         let stringDate: string = "";
//         if (date) {
//             stringDate += date.year + "-";
//             stringDate += isNumber(date.month) ? padNumber(date.month) + "-" : "";
//             stringDate += isNumber(date.day) ? padNumber(date.day) : ""

//         }
//         return stringDate;
//     }
// }
//Code For Parsing the ngbDateFormat ends here