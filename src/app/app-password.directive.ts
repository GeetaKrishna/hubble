import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAppPassword]'
})
export class AppPasswordDirective {
  private _shown = false;
  constructor(private el: ElementRef) {  
    this.setup();
  }
  toggle(span: HTMLElement) {
    this._shown = !this._shown;
    if (this._shown) {
      this.el.nativeElement.setAttribute('type', 'text');
      span.setAttribute('class', 'fa fa-eye-slash fa-lg');

    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      span.setAttribute('class', 'fa fa-eye fa-lg');
    }
  }

  setup() {
    const parent = this.el.nativeElement.parentNode;
    const span = document.createElement('i');
    span.setAttribute('class', 'fa fa-eye fa-lg');
  //   {
  //     position: absolute;
  //     right: 6px;
  //     top: 27px;
  // } 
  span.setAttribute('style','position: absolute; right: 6px; top: 27px')
  span.addEventListener('click', (event) => {
      this.toggle(span);
    });
    parent.appendChild(span);
}
}
