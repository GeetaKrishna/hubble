import { Routes } from '@angular/router';

//Layouts
import {
  CondensedComponent,
  BlankComponent,
  CorporateLayout,
  SimplyWhiteLayout,
  ExecutiveLayout,
  CasualLayout,
  BlankCasualComponent,
  BlankCorporateComponent,
  BlankSimplywhiteComponent
} from './@pages/layouts';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { EmployeeSearchComponent } from './components/employee-search/employee-search.component';
import { LockComponent } from './components/lock/lock.component';
import { LeavesComponent } from './components/leaves/leaves.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HierarchyComponent } from './components/hierarchy/hierarchy.component';
import { EmployeeSearchOneComponent } from './components/employee-search-one/employee-search-one.component';
import { ViewProfileComponent } from './components/view-profile/view-profile.component';
import { ProfileNewComponent } from './components/profile-new/profile-new.component';
import { HeirTwoComponent } from './components/heir-two/heir-two.component';
//import { HierarchyThreeComponent } from './components/hierarchy-three/hierarchy-three.component';
import { EmployeeSearch3Component } from './components/employee-search3/employee-search3.component';
import { TimesheetsComponent } from './components/timesheets/timesheets.component';
import { TimesheetsTwoComponent } from './components/timesheets-two/timesheets-two.component';
import { TimesheetsThreeComponent } from './components/timesheets-three/timesheets-three.component';
import { LeaveApprovalsComponent } from './components/leave-approvals/leave-approvals.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { BmsComponent } from './components/bms/bms.component';
import { TimesheetsApprovalComponent } from './components/timesheets-approval/timesheets-approval.component';
import { StarPerformersComponent } from './components/star-performers/star-performers.component';
import { AuthGuard } from './guards/auth.guard';
import { BiometricComponent } from './components/biometric/biometric.component';


export const AppRoutes: Routes = [
    {
    path: '',
    component: BlankCorporateComponent,
    children: [
      {
        path: '',
        component: LoginComponent
      }
    ]
  },
  {
    path: 'corporate',
    data: {
      breadcrumb: 'Home'
    },
    component: CorporateLayout
  },
  {
    path: 'register',
    component: BlankCorporateComponent,
    children: [
      {
        path: '',
        component: RegisterComponent
      }
    ]
  },
  {
    path: 'lock',
    component: BlankCorporateComponent,
    children: [
      {
        path: '',
        component: LockComponent
      },

    ]
  },
{
    path: 'forgot-password',
    component: BlankCorporateComponent,
    children: [
      {
        path: '',
        component: ForgotPasswordComponent
      }
    ]
  },
  {
    path: 'dashboard',
    component: CorporateLayout,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'leaves',
        children: [
          {
            path: '',
            component: LeavesComponent
          }
        ]
      },
      {
        path: 'calendar',
        component: CalendarComponent
      },
      {
        path: 'star-performers',
        component: StarPerformersComponent
      },
      {
        path: 'timesheets-2',
        component: TimesheetsTwoComponent
      },
      {
        path: 'timesheets-approvals',
        component: TimesheetsApprovalComponent
      },
      {
        path: 'timesheets-3',
        component: TimesheetsThreeComponent
      },
      {
       path: 'timesheets',
       component: TimesheetsComponent
     },{
      path: 'employee-search',
      component: EmployeeSearchComponent
    },
    {
      path: 'employee-search-one',
      component: EmployeeSearchOneComponent
    },
    {
      path: 'employee-search-three',
      component: EmployeeSearch3Component
    },
    {
      path: 'view-profile',
      component: ViewProfileComponent
    },{
      path: 'my-profile',
      component: ProfileNewComponent
    },{
      path: 'hierarchy-2',
      component: HeirTwoComponent
    },
      {
        path: 'hierarchy',
        component: HierarchyComponent
      }/* , {
        path: 'hierarchy-3',
        component: HierarchyThreeComponent
      } */,{
        path: 'bms',
        component: BmsComponent
      }, {
        path: 'leave-approvals',
        component: LeaveApprovalsComponent
      },{
        path: 'biometric',
        data: {
          breadcrumb: 'biometric'
        },
        component: BiometricComponent
      }]
  }
];
